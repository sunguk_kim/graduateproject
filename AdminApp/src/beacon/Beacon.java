package beacon;

import com.example.adminapp.R;
import com.example.adminapp.R.drawable;
import com.example.adminapp.R.id;
import com.example.adminapp.R.menu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

/*class for beacon data*/
public class Beacon extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.beacon);
		setContentView(new DrawView(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	  public boolean onTouchEvent(MotionEvent event) {
		    if (event.getAction() == MotionEvent.ACTION_DOWN){
		    	if(event.getX() > 80 && event.getX() < 200 && event.getY() > 1700 && event.getY() < 1800){
		    		Toast.makeText(this, "back work", Toast.LENGTH_SHORT).show();
		    		finish();
		    	}
		     
		    }
		    return super.onTouchEvent(event);
		   }
private class DrawView extends View{
	Bitmap map;
	Bitmap backBtn;
	Paint mPaint;
	
	DrawView(Context context){
		super(context);
		
		WindowManager windowManager = (WindowManager)getSystemService(Context.WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();

		map = BitmapFactory.decodeResource(getResources(), R.drawable.resize2_rotate_map_back);
		map = Bitmap.createScaledBitmap(map,display.getWidth(),display.getHeight(), true);
		
		backBtn = BitmapFactory.decodeResource(getResources(), R.drawable.arrow_back);
		backBtn = Bitmap.createScaledBitmap(backBtn, display.getWidth()/6, display.getHeight()/8, true);
		
		mPaint = new Paint();
	}
	 protected void onDraw(Canvas canvas) {
            canvas.drawBitmap(map, 0, 0,null);
 
            mPaint.setColor(Color.RED);
            
            canvas.drawCircle(311, 900, 15, mPaint);
            canvas.drawCircle(535, 450 , 15 , mPaint);
            canvas.drawCircle(560, 1600, 15, mPaint);  
            
            canvas.drawBitmap(backBtn, 50, 1600 ,null);
            
        }

}
}
