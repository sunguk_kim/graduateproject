package customs;

import java.util.ArrayList;

import com.example.adminapp.R;
import com.example.adminapp.R.id;
import com.example.adminapp.R.layout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/*class for custom list view adapter
 * it present 2 text view*/
public class ScheduleListCustomAdapter extends ArrayAdapter<ScheduleStore> {

	private ArrayList<ScheduleStore> list;
	private Context context;
	private LayoutInflater mInflater;

	public ScheduleListCustomAdapter(Context c, int textViewResourceId,
			ArrayList<ScheduleStore> data) {
		super(c, textViewResourceId, data);

		context = c;
		mInflater = LayoutInflater.from(c);
		list = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.custom_listview_layout, null);
		}
		
		ScheduleStore st = list.get(position);
		if(st!=null){
			TextView date = (TextView)v.findViewById(R.id.customListTime);
			TextView description = (TextView)v.findViewById(R.id.customListToDo);
			
			if(date!= null && description != null){
				date.setText(st.getDate());
				description.setText(st.getDescription());
			}
		}
		return v;
	}

	public void setArrayList(ArrayList<ScheduleStore> data) {
		list = data;
	}

	public ArrayList<ScheduleStore> getArrayList() {
		return list;
	}

}
