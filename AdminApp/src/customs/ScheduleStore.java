package customs;


/*class for support scheduleListCustomAdapter
 * it store data
 * */
public class ScheduleStore {
	String date;
	String description;
	
	public ScheduleStore(String date, String description){
		this.date=date;
		this.description=description;
	}
	public String getDate(){
		return date;
	}
	public String getDescription(){
		return description;
	}
}
