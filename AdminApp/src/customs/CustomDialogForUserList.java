package customs;

import com.example.adminapp.R;
import com.example.adminapp.R.id;
import com.example.adminapp.R.layout;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/*class for CustomDialog for present user list*/
public class CustomDialogForUserList extends Dialog {

	private TextView title;
	private ListView userList;
	private int count=0;
	private android.view.View.OnClickListener dClickListener;
	
	public CustomDialogForUserList(Context context, android.view.View.OnClickListener singleListener){
		super(context,android.R.style.Theme_Translucent_NoTitleBar);
		this.dClickListener = singleListener;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount=0.8f;
		getWindow().setAttributes(lpWindow);
		
		setContentView(R.layout.custom_dialog_for_user_list);
		
		title=(TextView)findViewById(R.id.userList_title_count_textView);
		title.setText(count);
		userList = (ListView)findViewById(R.id.userList_listView);
		
		Button btn = (Button)findViewById(R.id.userList_backBtn);
		if(dClickListener != null){
			btn.setOnClickListener(dClickListener);
		}
	}

	
}
