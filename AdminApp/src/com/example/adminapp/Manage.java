package com.example.adminapp;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import customs.ScheduleListCustomAdapter;
import customs.ScheduleStore;

/*class for individual data
 * it present user list with id
 * */
public class Manage extends Activity {

	private ListView nameList;
	ArrayAdapter<ScheduleStore> nameAdapter;
	String name = "";
	String id = "";
	String CODE = "";
	ArrayList<String> data;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage);

		StrictMode.enableDefaults();

		String[] st = { "USERLIST", "GETUSER" };
		GetUserListAsync as = new GetUserListAsync();
		as.execute(st);

	}

	private class GetUserListAsync extends
			AsyncTask<String[], Integer, Integer> {

		ProgressDialog pd;

		@Override
		protected Integer doInBackground(String[]... params) {
			// TODO Auto-generated method stub
			SendMessage smg = new SendMessage();

			data.add(smg.SendMessages(params[0][0]));
			data.add(smg.SendMessages(params[0][1]));

			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO 작업 종료후에 호출되지만, cancel할 경우에는 호출되지 않는다.
			super.onPostExecute(result);
			pd.dismiss();
			Log.e("TAG", data.get(0).toString() + " : "
					+ data.get(1).toString());
			ArrayList<ScheduleStore> names = new ArrayList<ScheduleStore>();
			name = data.get(0).substring(39, data.get(0).length() - 4);
			id = data.get(1).substring(38, data.get(1).length() - 4);
			StringTokenizer stk = new StringTokenizer(name, ",");
			StringTokenizer stk2 = new StringTokenizer(id, ",");

			while (stk.hasMoreTokens() && stk2.hasMoreElements()) {

				ScheduleStore st;
				st = new ScheduleStore(stk.nextToken(), stk2.nextToken());
				names.add(st);
			}
			nameAdapter = new ScheduleListCustomAdapter(Manage.this,
					R.layout.custom_listview_layout, names);

			nameList = (ListView) findViewById(R.id.nameList);

			nameList.setAdapter(nameAdapter);
			nameList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			nameList.setDivider(new ColorDrawable(Color.WHITE));
			nameList.setDividerHeight(2);

			nameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub

					Toast.makeText(
							getApplicationContext(),
							((TextView) view.findViewById(R.id.customListToDo))
									.getText().toString(), Toast.LENGTH_SHORT)
							.show();
					Intent i = new Intent(Manage.this, IndividualStatus.class);
					i.putExtra("NAME", ((TextView) view
							.findViewById(R.id.customListToDo)).getText()
							.toString());
					i.putExtra("USERNAME", ((TextView) view
							.findViewById(R.id.customListTime)).getText()
							.toString());
					startActivity(i);
					finish();

				}

			});
			Button btn = (Button) findViewById(R.id.manage_backBtn);
			btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}

			});
		}

		@Override
		protected void onPreExecute() {
			// TODO 작업 시작전에 호출된다.
			super.onPreExecute();
			pd = ProgressDialog.show(Manage.this, "", "잠시 기다려주세요", true);
			data = new ArrayList<String>();

		}

	}
}