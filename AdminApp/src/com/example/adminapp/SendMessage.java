package com.example.adminapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/*class for sendMsg to server*/
//@SuppressWarnings("deprecation")
public class SendMessage { // admin
	SendMessage() {
	}

	String SendMessages(String msg) {
		String result;
		result = SendByHttp(msg);

		Log.e("sendByHttp", "send result in sendmessage class : " + result);
		String[][] parsedData = jsonParserList(result);
		if (result.equals("timeout"))
			Log.e("sendbyHttp", "timeout");
		else
			return result;
		return null;

	}

	private String SendByHttp(String msg) {
		Log.e("sendbyHttp", msg);
		if (msg == null)
			msg = "";

		String URL = "http://172.16.38.177:8181/MemberManager/service.jsp";

		DefaultHttpClient client = new DefaultHttpClient();
		try {
			/* message 서버로 전송 */
			HttpPost post = new HttpPost(URL + "?msg=" + msg);

			StringEntity entity = new StringEntity(msg, HTTP.UTF_8);
			post.setEntity(entity);

			Log.e("sendbyHttp", "send " + msg); // protocol에 따라 response값 처리
			/* 지연시간 최대 5초 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);

			/* 데이터 보낸 뒤 서버에서 데이터를 받아오는 과정 */
			HttpResponse response = client.execute(post);
			BufferedReader bufreader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent(),
							"UTF-8"));

			String line = null;
			String result = "";
			while ((line = bufreader.readLine()) != null) {
				result += line;
			}

			Log.e("sendbyHttp", "get resultsssss : " + result);
			return result;
		} catch (Exception e) {
			Log.e("sendbyHttp", "exception occur " + e.toString());
			e.printStackTrace();
			client.getConnectionManager().shutdown(); // 연결 지연 종료
			return "timeout";
		}

	}

	/**
	 * 받은 JSON 객체를 파싱하는 메소드
	 * 
	 * @param page
	 * @return
	 */
	private String[][] jsonParserList(String pRecvServerPage) {

		Log.e("sendbyHttp", "서버에서 받은 전체 내용 : " + pRecvServerPage);

		try {
			JSONObject json = new JSONObject(pRecvServerPage);
			JSONArray jArr = json.getJSONArray("List");

			// 받아온 pRecvServerPage를 분석하는 부분
			String[] jsonName = { "msg1", "msg2", "msg3" };
			String[][] parseredData = new String[jArr.length()][jsonName.length];
			for (int i = 0; i < jArr.length(); i++) {
				json = jArr.getJSONObject(i);

				for (int j = 0; j < jsonName.length; j++) {
					parseredData[i][j] = json.getString(jsonName[j]);
				}
			}

			// 분해 된 데이터를 확인하기 위한 부분
			for (int i = 0; i < parseredData.length; i++) {
				Log.i("JSON을 분석한 데이터 " + i + " : ", parseredData[i][0]);
				Log.i("JSON을 분석한 데이터 " + i + " : ", parseredData[i][1]);
				Log.i("JSON을 분석한 데이터 " + i + " : ", parseredData[i][2]);
			}

			return parseredData;
		} catch (JSONException e) {
			e.printStackTrace();
		} 
		return null;
	}

}
