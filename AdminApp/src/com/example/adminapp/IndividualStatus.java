package com.example.adminapp;

import java.util.ArrayList;
import java.util.StringTokenizer;

import customs.ScheduleListCustomAdapter;
import customs.ScheduleStore;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/*class for individual data
 * disease, name, birth, schedule, extra phone numbers*/
public class IndividualStatus extends Activity {
	private ListView schedule;
	ScheduleListCustomAdapter nameAdapter;

	TextView nameText, secondPhoneNumber, diseaseText, birth;
	private String result = "";
	private String name = "";
	private String userName = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.individual_layout);
		schedule = (ListView) findViewById(R.id.scheduleList);
		bindWidgets();
		name = getIntent().getStringExtra("NAME");
		userName = getIntent().getStringExtra("USERNAME");
		GetInfo gi = new GetInfo();
		gi.run();

		ArrayList<ScheduleStore> names = new ArrayList<ScheduleStore>();

		result = result.substring(39);

		nameText.setText(userName); // ����Ʈ���� �̸��� �ҷ���
		StringTokenizer stk = new StringTokenizer(result, ";");
		Log.e("TAG", "start tokenize");
		int i = 0;
		while (stk.hasMoreTokens()) {
			if (i == 1) {
				String tmp = stk.nextToken();
				Log.e("TAG", tmp);
				if (tmp.length() > 15)
					secondPhoneNumber.setText(" ");
				else
					secondPhoneNumber.setText(tmp);
				i++;
			}
			if (i == 0) {
				String tmp = stk.nextToken();
				birth.setText(tmp);

				i++;
			}
			if (i == 2) {
				diseaseText.setText(stk.nextToken());
				i++;
			}
			if (i == 3)
				break;
		}
		String scheduleData = stk.nextToken();
		scheduleData = scheduleData.substring(0, scheduleData.length() - 4);

		stk = new StringTokenizer(scheduleData, "#");

		while (stk.hasMoreTokens()) {
			String tmp = stk.nextToken();

			Log.e("TAG", "value" + tmp);
			if (tmp.contains("null"))
				continue;

			ScheduleStore st = new ScheduleStore(
					tmp.substring(0, 2) + " : 00 ", tmp.substring(2));
			names.add(st);
		}

		nameAdapter = new ScheduleListCustomAdapter(this,
				R.layout.custom_listview_layout, names);

		schedule.setAdapter(nameAdapter);
		schedule.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		schedule.setDivider(new ColorDrawable(Color.WHITE));
		schedule.setDividerHeight(2);

		Button backBtn = (Button) findViewById(R.id.ind_backBtn);
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}

		});

	}

	private void bindWidgets() {
		nameText = (TextView) findViewById(R.id.nameText);
		secondPhoneNumber = (TextView) findViewById(R.id.seconPhoneNumber);
		diseaseText = (TextView) findViewById(R.id.diseaseText);
		birth = (TextView) findViewById(R.id.textView4);

	}

	private class GetInfo implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			SendMessage smg = new SendMessage();
			result = smg.SendMessages("INFO" + name);
		}

	}

}
