package com.example.adminapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/*class for present homepage*/
public class Homepage extends Activity {
	WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homepage);
		webView = (WebView) findViewById(R.id.webview);
		 webView.loadUrl("http://192.168.0.125:8181/MemberManager/index.jsp");

		webView.setWebViewClient(new WebClient());
		webView.getSettings().setJavaScriptEnabled(true);
		//webView.loadUrl("http://inven.co.kr");
	}



	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {Log.e("TEst","longpress");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.e("TEst","longpress");
			return true;
		}
		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {

			webView.goBack();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	

	
	@Override
	// 종료처리시 종료 할지 물어보기 추가
	public void onBackPressed() {
		new AlertDialog.Builder(this).setTitle("종료").setMessage("종료 하시겠습니까?")
				.setNegativeButton("아니요", null)
				.setPositiveButton("예", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					}
				}).show();
	}

	class WebClient extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

}
