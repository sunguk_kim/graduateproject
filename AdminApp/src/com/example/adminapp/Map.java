package com.example.adminapp;

import java.util.ArrayList;
import java.util.StringTokenizer;

import customs.ScheduleListCustomAdapter;
import customs.ScheduleStore;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.Toast;

/*class for current situation
 * it present users and warning area
 * */
public class Map extends Activity { // alert가 있는경우 확인하고 receive

	SendMessage sendMsg;
	String alertPos = null; // get alertArea
	String name = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();

		StrictMode.setThreadPolicy(policy);
		sendMsg = new SendMessage();

		alertPos = sendMsg.SendMessages("ISALERT"); // alert확인
		Log.e("TAG", "alertpos : " + alertPos);

		if (alertPos != null)
			alertPos = alertPos.subSequence(42, 46).toString();

		Log.e("sendbyHttp", "Alert position : " + alertPos);

		setContentView(new DrawView(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (event.getX() > 80 && event.getX() < 200 && event.getY() > 1700
					&& event.getY() < 1800) {
				// Toast.makeText(this, "back work", Toast.LENGTH_SHORT).show();
				finish();
			}
			if (event.getX() >= 470 && event.getX() <= 570
					&& event.getY() >= 380 && event.getY() <= 630)// 4359
			{

				View dialog = View.inflate(getApplicationContext(),
						R.layout.custom_dialog_for_user_list, null);
				final AlertDialog ad = new AlertDialog.Builder(Map.this)
						.setView(dialog).create();

				final ListView ls = (ListView) dialog
						.findViewById(R.id.userList_listView);
				ScheduleListCustomAdapter nameAdapter = null;
				//

				String msg = "AREA" + 4359;
				name = sendMsg.SendMessages(msg);
				// db로 부터 정보 받아오기
				if (name.length() > 42) {

					name = name.substring(38, name.length() - 4);
					ArrayList<ScheduleStore> names = new ArrayList<ScheduleStore>();
					StringTokenizer stk = new StringTokenizer(name, ",");

					while (stk.hasMoreTokens()) {
						ScheduleStore st;
						st = new ScheduleStore("", stk.nextToken());
						names.add(st);
					}
					nameAdapter = new ScheduleListCustomAdapter(this,
							R.layout.custom_listview_layout, names);

					Log.e("TAG", ls.toString());

					ls.setAdapter(nameAdapter);
					ls.setChoiceMode(ListView.CHOICE_MODE_NONE);
					ls.setDivider(new ColorDrawable(Color.WHITE));
					ls.setDividerHeight(2);
				}
				dialog.findViewById(R.id.userList_backBtn).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ad.dismiss();
							}

						});
				ad.show();
			}
			if (event.getX() >= 180 && event.getX() < 300
					&& event.getY() >= 230 && event.getY() <= 1060)// 4358
			{
				View dialog = View.inflate(getApplicationContext(),
						R.layout.custom_dialog_for_user_list, null);
				final AlertDialog ad = new AlertDialog.Builder(Map.this)
						.setView(dialog).create();

				final ListView ls = (ListView) dialog
						.findViewById(R.id.userList_listView);
				ScheduleListCustomAdapter nameAdapter = null;
				//

				// TODO Auto-generated method stub
				String msg = "AREA" + 4358;
				name = sendMsg.SendMessages(msg);
				if (name.length() > 42) {

					// db로 부터 정보 받아오기
					name = name.substring(38, name.length() - 4);
					ArrayList<ScheduleStore> names = new ArrayList<ScheduleStore>();
					StringTokenizer stk = new StringTokenizer(name, ",");

					while (stk.hasMoreTokens()) {
						ScheduleStore st;
						st = new ScheduleStore("", stk.nextToken());
						names.add(st);
					}
					nameAdapter = new ScheduleListCustomAdapter(this,
							R.layout.custom_listview_layout, names);

					Log.e("TAG", ls.toString());

					ls.setAdapter(nameAdapter);
					ls.setChoiceMode(ListView.CHOICE_MODE_NONE);
					ls.setDivider(new ColorDrawable(Color.WHITE));
					ls.setDividerHeight(2);
				}
				dialog.findViewById(R.id.userList_backBtn).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub

								ad.dismiss();
							}

						});
				ad.show();
			}
			if (event.getX() >= 480 && event.getX() <= 580
					&& event.getY() >= 1560 && event.getY() <= 1820)// 4360
			{
				View dialog = View.inflate(getApplicationContext(),
						R.layout.custom_dialog_for_user_list, null);
				final AlertDialog ad = new AlertDialog.Builder(Map.this)
						.setView(dialog).create();

				final ListView ls = (ListView) dialog
						.findViewById(R.id.userList_listView);
				ScheduleListCustomAdapter nameAdapter = null;
				//

				String msg = "AREA" + 4360;
				name = sendMsg.SendMessages(msg);
				if (name.length() > 42) {
					// db로 부터 정보 받아오기

					name = name.substring(38, name.length() - 4);
					ArrayList<ScheduleStore> names = new ArrayList<ScheduleStore>();
					StringTokenizer stk = new StringTokenizer(name, ",");

					while (stk.hasMoreTokens()) {
						ScheduleStore st;
						st = new ScheduleStore("", stk.nextToken());
						names.add(st);
					}

					nameAdapter = new ScheduleListCustomAdapter(this,
							R.layout.custom_listview_layout, names);

					Log.e("TAG", ls.toString());

					ls.setAdapter(nameAdapter);
					ls.setChoiceMode(ListView.CHOICE_MODE_NONE);
					ls.setDivider(new ColorDrawable(Color.WHITE));
					ls.setDividerHeight(2);
				}
				dialog.findViewById(R.id.userList_backBtn).setOnClickListener(
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								ad.dismiss();
							}

						});
				ad.show();
			}
		}
		name = "";
		return super.onTouchEvent(event);
	}

	private class DrawView extends View {
		Bitmap map;
		Paint mPaint;
		Bitmap warning;
		Bitmap person4358;
		Bitmap person4359;
		Bitmap person4360;
		Bitmap backBtn;

		DrawView(Context context) {
			super(context);

			WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
			Display display = windowManager.getDefaultDisplay();

			map = BitmapFactory.decodeResource(getResources(),
					R.drawable.resize2_rotate_map_back);
			map = Bitmap.createScaledBitmap(map, display.getWidth(),
					display.getHeight(), true);
			warning = BitmapFactory.decodeResource(getResources(),
					R.drawable.warning);
			warning = Bitmap.createScaledBitmap(warning,
					display.getWidth() / 8, display.getHeight() / 16, true);
			person4358 = BitmapFactory.decodeResource(getResources(),
					R.drawable.person);
			person4358 = Bitmap.createScaledBitmap(person4358,
					display.getWidth() / 4, display.getHeight() / 8, true);

			person4359 = BitmapFactory.decodeResource(getResources(),
					R.drawable.person);
			person4359 = Bitmap.createScaledBitmap(person4359,
					display.getWidth() / 4, display.getHeight() / 8, true);

			person4360 = BitmapFactory.decodeResource(getResources(),
					R.drawable.person);
			person4360 = Bitmap.createScaledBitmap(person4360,
					display.getWidth() / 4, display.getHeight() / 8, true);

			backBtn = BitmapFactory.decodeResource(getResources(),
					R.drawable.arrow_back);
			backBtn = Bitmap.createScaledBitmap(backBtn,
					display.getWidth() / 6, display.getHeight() / 8, true);

			mPaint = new Paint();
		}

		protected void onDraw(Canvas canvas) {
			canvas.drawBitmap(map, 0, 0, null);

			canvas.drawBitmap(person4358, 100, 750, null);
			canvas.drawBitmap(person4359, 400, 300, null);
			canvas.drawBitmap(person4360, 400, 1500, null);

			if (alertPos != null) { // edit
				if (alertPos.equals("4358"))
					canvas.drawBitmap(warning, 200, 800, null); // 문쪽 4358
				else if (alertPos.equals("4359"))
					canvas.drawBitmap(warning, 450, 300, null); // 거실 4359
				else if (alertPos.equals("4360"))
					canvas.drawBitmap(warning, 500, 1500, null); // 침대 4360
			} else {
			}
			canvas.drawBitmap(backBtn, 50, 1600, null);
			invalidate();
		}

	}
}