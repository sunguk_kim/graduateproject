package com.example.adminapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import beacon.Beacon;

import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends Activity {
	private String GCMcode = "";

	private SendMessage sendMsg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		ImageButton manage = (ImageButton) findViewById(R.id.Manage);
		ImageButton map = (ImageButton) findViewById(R.id.Map);
		ImageButton homepage = (ImageButton) findViewById(R.id.Homepage);
		ImageButton beacon = (ImageButton) findViewById(R.id.Beacon);

		manage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Manage.class);
				// Intent intent=new Intent(MainActivity.this,
				// IndividualStatus.class);

				startActivity(intent);
			}
		});

		map.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(MainActivity.this, Map.class);
				startActivity(intent);
			}
		});

		homepage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Homepage.class);
				startActivity(intent);
			}
		});

		beacon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this, Beacon.class);
				startActivity(intent);
			}
		});

		registerGcm();
	}

	public void registerGcm() { // add regID for push message
		SharedPreferences pref = getSharedPreferences("GCMCODE",
				Activity.MODE_PRIVATE);

		GCMRegistrar.checkDevice(this);

		GCMRegistrar.checkManifest(this);
		TelephonyManager telManager = (TelephonyManager) this
				.getSystemService(this.TELEPHONY_SERVICE);
		final String phoneNum = telManager.getLine1Number();

		Log.e("TAG", phoneNum);
		final String regId = GCMRegistrar.getRegistrationId(this);
		SharedPreferences.Editor edit = pref.edit();
		Log.e("TAG",regId+"before if else");
		sendMsg = new SendMessage();

		if (regId.equals("")) {
			
			GCMRegistrar.register(this, "1063402905494");
			edit.putString("GCMCODE", "if sequence"+regId);
			edit.commit();

			Log.e("TAG", regId);
			new Thread(new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					String msg = "GCMC" + regId + "PHON" + phoneNum;
					sendMsg.SendMessages(msg);
				}
			}).start();
			Log.e("TAG", regId);
		} else { // regId already exist
			Log.e("id", regId);
			GCMRegistrar.register(this, "1063402905494");
			if (!regId.equals(pref.getString("GCMCode", ""))) { // not equal
																// with pref
				edit.putString("GCMCODE", regId);
				edit.commit();
				Log.e("TAG", "else sequence"+regId);

				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						String msg = "GCMC" + regId + "PHON" + phoneNum;
						sendMsg.SendMessages(msg);
					}
				}).start();
			} else
				return;

		}

	}
}