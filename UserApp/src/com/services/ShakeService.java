package com.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.Vibrator;
import android.widget.Toast;

import com.example.userapp.MainActivity;
import com.example.userapp.SendMessage;

/*class for shake service. this function for blind person*/
public class ShakeService extends Service implements SensorEventListener {

	 private long lastTime;
	    private float speed;
	    private float lastX;
	    private float lastY;
	    private float lastZ;
	    private float x, y, z;
	    private int i =0 ;
	    private static final int SHAKE_THRESHOLD = 2000;
	    private static final int DATA_X = SensorManager.DATA_X;
	    private static final int DATA_Y = SensorManager.DATA_Y;
	    private static final int DATA_Z = SensorManager.DATA_Z;
	 
	    private SensorManager sensorManager;
	    private Sensor accelerormeterSensor;
	 
	    @Override
		public void onCreate() {
	        super.onCreate();
	        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	        accelerormeterSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	        //Toast.makeText(this,"Service create!",Toast.LENGTH_SHORT).show();
	    }
	    public int onStartCommand(Intent intent, int flags, int startId) {
	        super.onStartCommand(intent, flags, startId);
	     
	        if (accelerormeterSensor != null)
	            sensorManager.registerListener(this, accelerormeterSensor,
	            SensorManager.SENSOR_DELAY_GAME);
	        return START_REDELIVER_INTENT;
	    }

		@Override
		public boolean stopService(Intent name) {
			// TODO Auto-generated method stub
	        if (sensorManager != null)
	            sensorManager.unregisterListener(this);
			return super.stopService(name);
		}
	    @Override
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	 
	    }
	 
	    @Override
	    public void onSensorChanged(SensorEvent event) {
	        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
	            long currentTime = System.currentTimeMillis();
	            long gabOfTime = (currentTime - lastTime);
	            if (gabOfTime > 100) {
	                lastTime = currentTime;
	                x = event.values[SensorManager.DATA_X];
	                y = event.values[SensorManager.DATA_Y];
	                z = event.values[SensorManager.DATA_Z];
	 
	                speed = Math.abs(x + y + z - lastX - lastY - lastZ) / gabOfTime * 10000;
	 
	                if (speed > SHAKE_THRESHOLD) { //shake Event Occur!
	                	String area = BackGroundScan.areaOnService;
	                	SendMessage sendMsg;
	                	String exist=BackGroundScan.UserIDOnService;
	                	
	                	String msgToServer = "POSI"+area+"ALERT"+exist;
	                	sendMsg = new SendMessage();
	    				String clickSOS = sendMsg.SendMessages(msgToServer);
	    				Toast.makeText(this, msgToServer,
	    						Toast.LENGTH_LONG).show();
	    				final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
	    				vibrator.vibrate(1000);
	                	//Toast.makeText(this, (++i)+ "SHAKING!", Toast.LENGTH_SHORT).show();
	                }
	 
	                lastX = event.values[DATA_X];
	                lastY = event.values[DATA_Y];
	                lastZ = event.values[DATA_Z];
	            }
	 
	        }
	 
	    }

		@Override
		public IBinder onBind(Intent intent) {
			// TODO Auto-generated method stub
			return null;
		}

}
