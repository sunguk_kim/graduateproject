/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014-2015 Perples, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.services;

import java.util.ArrayList;
import java.util.Collection;

import Beacon.IdentifyArea;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.example.userapp.MainActivity;
import com.example.userapp.SendMessage;
import com.perples.recosdk.RECOBeacon;
import com.perples.recosdk.RECOBeaconManager;
import com.perples.recosdk.RECOBeaconRegion;
import com.perples.recosdk.RECOBeaconRegionState;
import com.perples.recosdk.RECOErrorCode;
import com.perples.recosdk.RECOMonitoringListener;
import com.perples.recosdk.RECORangingListener;
import com.perples.recosdk.RECOServiceConnectListener;

/*class for background service it contains send msg and alert signal when stay warning area over warning time*/
/**
 * RECOBackgroundRangingService is to monitor regions and range regions when the
 * device is inside in the BACKGROUND.
 * 
 * RECOBackgroundMonitoringService��諛깃렇�쇱슫�쒖뿉��monitoring���섑뻾�섎ŉ, �뱀젙 region
 * �대�濡�吏꾩엯��寃쎌슦 諛깃렇�쇱슫���곹깭�먯꽌 ranging���섑뻾�⑸땲��
 */
public class BackGroundScan extends Service implements RECOMonitoringListener,
		RECORangingListener, RECOServiceConnectListener {

	/**
	 * We recommend 1 second for scanning, 10 seconds interval between scanning,
	 * and 60 seconds for region expiration time. 1珥��ㅼ틪, 10珥�媛꾧꺽�쇰줈 �ㅼ틪, 60珥덉쓽
	 * region expiration time���뱀궗 沅뚯옣�ы빆�낅땲��
	 */
	private long mScanDuration = 10 * 1000L;
	private long mSleepDuration = 100 * 1000L;
	private long mRegionExpirationTime = 300 * 1000L;
	private final int warningArea = 4358; // set warning area
	private final int warningTime = 30; // set term for notice warning
	private int hits = 0;

	private RECOBeaconManager mRecoManager;
	private ArrayList<RECOBeaconRegion> mRegions;

	static String areaOnService = "";
	static String UserIDOnService = "";

	@Override
	public void onCreate() {
		Log.i("RECOBackgroundRangingService", "onCreate()");
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i("RECOBackgroundRangingService", "onStartCommand");
		/**
		 * Create an instance of RECOBeaconManager (to set scanning target and
		 * ranging timeout in the background.) If you want to scan only RECO,
		 * and do not set ranging timeout in the backgournd, create an instance:
		 * mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(),
		 * true, false); WARNING: False enableRangingTimeout will affect the
		 * battery consumption.
		 * 
		 * RECOBeaconManager �몄뒪�댁뒪���앹꽦�⑸땲�� (�ㅼ틪 ��긽 諛�諛깃렇�쇱슫��ranging timeout
		 * �ㅼ젙) RECO留뚯쓣 �ㅼ틪�섍퀬, 諛깃렇�쇱슫��ranging timeout���ㅼ젙�섍퀬 �띠� �딆쑝�쒕떎硫�
		 * �ㅼ쓬怨�媛숈씠 �앹꽦�섏떆湲�諛붾엻�덈떎. mRecoManager =
		 * RECOBeaconManager.getInstance(getApplicationContext(), true, false);
		 * 二쇱쓽: enableRangingTimeout��false濡��ㅼ젙 �� 諛고꽣由��뚮え�됱씠 利앷��⑸땲��
		 */
		mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(),
				MainActivity.SCAN_RECO_ONLY, false);
		this.bindRECOService();
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Log.i("RECOBackgroundRangingService", "onDestroy()");
		this.tearDown();
		super.onDestroy();
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Log.i("RECOBackgroundRangingService", "onTaskRemoved()");
		super.onTaskRemoved(rootIntent);
	}

	private void bindRECOService() {
		Log.i("RECOBackgroundRangingService", "bindRECOService()");

		mRegions = new ArrayList<RECOBeaconRegion>();
		this.generateBeaconRegion();

		mRecoManager.setMonitoringListener(this);
		mRecoManager.setRangingListener(this);
		mRecoManager.bind(this);
	}

	private void generateBeaconRegion() {
		Log.i("RECOBackgroundRangingService", "generateBeaconRegion()");

		RECOBeaconRegion recoRegion;

		recoRegion = new RECOBeaconRegion(MainActivity.RECO_UUID, "REGIONS");
		recoRegion.setRegionExpirationTimeMillis(this.mRegionExpirationTime);
		mRegions.add(recoRegion);
	}

	private void startMonitoring() {
		Log.i("RECOBackgroundRangingService", "startMonitoring()");

		mRecoManager.setScanPeriod(this.mScanDuration);
		mRecoManager.setSleepPeriod(this.mSleepDuration);

		for (RECOBeaconRegion region : mRegions) {
			try {
				mRecoManager.startMonitoringForRegion(region);
			} catch (RemoteException e) {
				Log.e("RECOBackgroundRangingService",
						"RemoteException has occured while executing RECOManager.startMonitoringForRegion()");
				e.printStackTrace();
			} catch (NullPointerException e) {
				Log.e("RECOBackgroundRangingService",
						"NullPointerException has occured while executing RECOManager.startMonitoringForRegion()");
				e.printStackTrace();
			}
		}
	}

	private void stopMonitoring() {
		Log.i("RECOBackgroundRangingService", "stopMonitoring()");

		for (RECOBeaconRegion region : mRegions) {
			try {
				mRecoManager.stopMonitoringForRegion(region);
			} catch (RemoteException e) {
				Log.e("RECOBackgroundRangingService",
						"RemoteException has occured while executing RECOManager.stopMonitoringForRegion()");
				e.printStackTrace();
			} catch (NullPointerException e) {
				Log.e("RECOBackgroundRangingService",
						"NullPointerException has occured while executing RECOManager.stopMonitoringForRegion()");
				e.printStackTrace();
			}
		}
	}

	private void startRangingWithRegion(RECOBeaconRegion region) {
		Log.i("RECOBackgroundRangingService", "startRangingWithRegion()");

		/**
		 * There is a known android bug that some android devices scan BLE
		 * devices only once. (link:
		 * http://code.google.com/p/android/issues/detail?id=65863) To resolve
		 * the bug in our SDK, you can use setDiscontinuousScan() method of the
		 * RECOBeaconManager. This method is to set whether the device scans BLE
		 * devices continuously or discontinuously. The default is set as FALSE.
		 * Please set TRUE only for specific devices.
		 * 
		 * mRecoManager.setDiscontinuousScan(true);
		 */

		try {
			mRecoManager.startRangingBeaconsInRegion(region);
		} catch (RemoteException e) {
			Log.e("RECOBackgroundRangingService",
					"RemoteException has occured while executing RECOManager.startRangingBeaconsInRegion()");
			e.printStackTrace();
		} catch (NullPointerException e) {
			Log.e("RECOBackgroundRangingService",
					"NullPointerException has occured while executing RECOManager.startRangingBeaconsInRegion()");
			e.printStackTrace();
		}
	}

	private void stopRangingWithRegion(RECOBeaconRegion region) {
		Log.i("RECOBackgroundRangingService", "stopRangingWithRegion()");

		try {
			mRecoManager.stopRangingBeaconsInRegion(region);
		} catch (RemoteException e) {
			Log.e("RECOBackgroundRangingService",
					"RemoteException has occured while executing RECOManager.stopRangingBeaconsInRegion()");
			e.printStackTrace();
		} catch (NullPointerException e) {
			Log.e("RECOBackgroundRangingService",
					"NullPointerException has occured while executing RECOManager.stopRangingBeaconsInRegion()");
			e.printStackTrace();
		}
	}

	private void tearDown() {
		Log.i("RECOBackgroundRangingService", "tearDown()");
		this.stopMonitoring();

		try {
			mRecoManager.unbind();
		} catch (RemoteException e) {
			Log.e("RECOBackgroundRangingService",
					"RemoteException has occured while executing unbind()");
			e.printStackTrace();
		}
	}

	@Override
	public void onServiceConnect() {
		Log.i("RECOBackgroundRangingService", "onServiceConnect()");
		this.startMonitoring();
		// Write the code when RECOBeaconManager is bound to RECOBeaconService
	}

	@Override
	public void didDetermineStateForRegion(RECOBeaconRegionState state,
			RECOBeaconRegion region) {
		Log.i("RECOBackgroundRangingService", "didDetermineStateForRegion()");
		// Write the code when the state of the monitored region is changed
		try {
			for (RECOBeaconRegion regions : mRegions)
				mRecoManager.startRangingBeaconsInRegion(regions);
		} catch (RemoteException e) {
		}

	}

	@Override
	public void didEnterRegion(RECOBeaconRegion region,
			Collection<RECOBeacon> beacons) {
		/**
		 * For the first run, this callback method will not be called. Please
		 * check the state of the region using didDetermineStateForRegion()
		 * callback method.
		 * 
		 * 理쒖큹 �ㅽ뻾�� ��肄쒕갚 硫붿냼�쒕뒗 �몄텧�섏� �딆뒿�덈떎. didDetermineStateForRegion()
		 * 肄쒕갚 硫붿냼�쒕� �듯빐 region �곹깭瑜��뺤씤�����덉뒿�덈떎.
		 */

		// Get the region and found beacon list in the entered region
		Log.i("RECOBackgroundRangingService",
				"didEnterRegion() - " + region.getUniqueIdentifier());
		// Write the code when the device is enter the region

		this.startRangingWithRegion(region); // start ranging to get beacons
												// inside of the region
		// from now, stop ranging after 10 seconds if the device is not exited

		DoScan(region, beacons);

	}

	private void DoScan(RECOBeaconRegion region, Collection<RECOBeacon> beacons) {
		Log.e("TAG", "DoTag on BGService");
		IdentifyArea ia = new IdentifyArea();
		int area = ia.whereArea(beacons);
		SharedPreferences pref = getSharedPreferences("USERID",
				Activity.MODE_PRIVATE);
		UserIDOnService = pref.getString("USERID", "");

		areaOnService = String.valueOf(area);
		if (area != 0) {
			SendMessage sendMsg = new SendMessage();
			String msgToServer = "NONE" + area + "NONE" + UserIDOnService;

			sendMsg.SendMessages(msgToServer);
			Log.e("TAG", msgToServer);

			if (area == 4358)
				checkUrgentArea();
			else {
				hits = 0;
			}
		}
	}

	@Override
	public void didExitRegion(RECOBeaconRegion region) {
		/**
		 * For the first run, this callback method will not be called. Please
		 * check the state of the region using didDetermineStateForRegion()
		 * callback method.
		 * 
		 * 理쒖큹 �ㅽ뻾�� ��肄쒕갚 硫붿냼�쒕뒗 �몄텧�섏� �딆뒿�덈떎. didDetermineStateForRegion()
		 * 肄쒕갚 硫붿냼�쒕� �듯빐 region �곹깭瑜��뺤씤�����덉뒿�덈떎.
		 */

		Log.i("RECOBackgroundRangingService",
				"didExitRegion() - " + region.getUniqueIdentifier());
		// Write the code when the device is exit the region

		this.stopRangingWithRegion(region); // stop ranging because the device
											// is outside of the region from now
	}

	@Override
	public void didStartMonitoringForRegion(RECOBeaconRegion region) {
		Log.i("RECOBackgroundRangingService",
				"didStartMonitoringForRegion() - "
						+ region.getUniqueIdentifier());
		// Write the code when starting monitoring the region is started
		// successfully
	}

	@Override
	public void didRangeBeaconsInRegion(Collection<RECOBeacon> beacons,
			RECOBeaconRegion region) {
		Log.i("RECOBackgroundRangingService", "didRangeBeaconsInRegion() - "
				+ region.getUniqueIdentifier() + " with " + beacons.size()
				+ " beacons");
		// Write the code when the beacons inside of the region is received
		Log.e("TAG", "DO SCAN ON DIDRANGEBEACONINREGION");
		if (beacons.size() > 0)
			DoScan(region, beacons);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// This method is not used
		return null;
	}

	@Override
	public void onServiceFail(RECOErrorCode errorCode) {
		// Write the code when the RECOBeaconService is failed.
		// See the RECOErrorCode in the documents.
		return;
	}

	@Override
	public void monitoringDidFailForRegion(RECOBeaconRegion region,
			RECOErrorCode errorCode) {
		// Write the code when the RECOBeaconService is failed to monitor the
		// region.
		// See the RECOErrorCode in the documents.
		return;
	}

	@Override
	public void rangingBeaconsDidFailForRegion(RECOBeaconRegion region,
			RECOErrorCode errorCode) {
		// Write the code when the RECOBeaconService is failed to range beacons
		// in the region.
		// See the RECOErrorCode in the documents.
		return;
	}

	public void checkUrgentArea() {
		hits++;
		Log.e("TAG", "Hits : " + hits);
		if (hits > warningTime) {
			SendMessage sendMsg = new SendMessage();
			String msgToServer = "POSI" + warningArea + "ALERT"
					+ UserIDOnService;
			sendMsg.SendMessages(msgToServer);
			Log.e("TAG", "Hits Over " + warningTime);
			hits = 0;
		}

	}
}
