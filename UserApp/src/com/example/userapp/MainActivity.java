package com.example.userapp;

import java.util.ArrayList;
import java.util.Collection;

import Beacon.IdentifyArea;
import Beacon.Point;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioAttributes;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.perples.recosdk.RECOBeacon;
import com.perples.recosdk.RECOBeaconManager;
import com.perples.recosdk.RECOBeaconRegion;
import com.perples.recosdk.RECOErrorCode;
import com.perples.recosdk.RECORangingListener;
import com.perples.recosdk.RECOServiceConnectListener;
import com.services.BackGroundScan;
import com.services.ShakeService;


/*class for MainActivity
 * 
 *	contains>>
 *	 login by id
 * 	 get beacon signal
 *   
 * 
 * */

public class MainActivity extends Activity implements RECORangingListener,
		RECOServiceConnectListener {
	public static final String RECO_UUID = "24DDF411-8CF1-440C-87CD-E368DAF9C93E";
	public static final boolean SCAN_RECO_ONLY = true;
	public static final boolean ENABLE_BACKGROUND_RANGING_TIMEOUT = true;
	public static final boolean DISCONTINUOUS_SCAN = false;
	public final String TAG = "UserApplication";
	RECOBeaconManager mRecoManager;
	ArrayList<RECOBeaconRegion> mRegions;
	private Point[] initBeaconPos = new Point[3]; // set initialize beacon pos
	private boolean isReady = false;
	public static Point trigulation = null; // 삼각측량 계싼값 저장
	private int beaconNumbers = 0;// 비콘 갯수
	private String msgToServer = ""; // sendMessage to server
	private SharedPreferences pref;
	int area = 0;
	SendMessage sendMsg;
	String exist = "";

	String sendResult = "";
	@Override
	// oncreate
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		sendMsg = new SendMessage();
		// preference for user id

		userInit();
		// pref 초기화
		/*
		 * pref = this.getSharedPreferences("USERID", this.MODE_PRIVATE);
		 * 
		 * SharedPreferences.Editor editor = pref.edit();
		 * 
		 * editor.clear();
		 * 
		 * editor.commit();
		 */
		// 초기화
		ImageButton sos = (ImageButton) findViewById(R.id.SOS);
		final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		StrictMode.enableDefaults();

		mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(),
				MainActivity.SCAN_RECO_ONLY,
				MainActivity.ENABLE_BACKGROUND_RANGING_TIMEOUT);
		mRegions = generateBeaconRegion();

		Log.e(TAG, "onCreate");
		BluetoothManager mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		BluetoothAdapter mBluetoothAdapter = mBluetoothManager.getAdapter();
		if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
			Intent enableBTIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBTIntent, 1);
		}

		connectTobeacon();
		 //startService(new Intent(this,BackGroundScan.class)); //background beacon scan service
		 startService(new Intent(this,ShakeService.class));// shake function as
		// service
		sos.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				msgToServer = "POSI" + area + "ALERT" + exist;
				
				new Thread(new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						String clickSOS = sendMsg.SendMessages(msgToServer);
					}
				}).start();
			//	Toast.makeText(MainActivity.this, msgToServer,
			//			Toast.LENGTH_LONG).show();
				
				final Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				
				vibrator.vibrate(300);

			}
		});
	}

	private void userInit() { // preferences확인 후 userid가 있으면 지나가고 아니면 입력
		// TODO Auto-generated method stub
		pref = getSharedPreferences("USERID", Activity.MODE_PRIVATE);
		exist = pref.getString("USERID", "");
		// SharedPreferences.Editor edit = pref.edit();

		if (!exist.isEmpty()) {
			Toast.makeText(this, pref.getString("USERID", "") + " : Welcome!",
					Toast.LENGTH_SHORT).show();
		} else {
			View dialog = View.inflate(getApplicationContext(),
					R.layout.customdialog, null);
			final AlertDialog ad = new AlertDialog.Builder(MainActivity.this)
					.setView(dialog).create();
			final EditText userID_dlg = (EditText) dialog
					.findViewById(R.id.userID_customDlog);
			dialog.findViewById(R.id.btn_customDlog).setOnClickListener(
					new OnClickListener() {

						@Override
						public void onClick(View v) { // dialog에서 확인 버튼 누를경우 id가
														// 있는지 확인
							// TODO Auto-generated method stub
							final String id = userID_dlg.getText().toString();
							Log.e("sendbyHttp", id);
							 SendMessage checkID = new SendMessage();
							 sendResult=checkID.SendMessages("aIDa" +id);
							
							/*new Thread(new Runnable() {
								@Override
								public void run() {
									// TODO Auto-generated method stub
									sendResult  =sendMsg.SendMessages("aIDa"
											+ id);
								}
							}).start();*/
							
							Log.e("sendbyHttp", "get send result : "
									+ sendResult);
							if (sendResult.contains("OK")) {// 있는경우
															// preferences에
															// 저장
								SharedPreferences.Editor edit = pref.edit();
								edit.putString("USERID", id);
								edit.commit();
								Log.e("POS", "ID check done!");
							} else {
								Toast.makeText(getApplicationContext(),
										"ID is not exist! check try again!",
										Toast.LENGTH_SHORT).show();
								finish();
							}
							Toast.makeText(getApplicationContext(),
									userID_dlg.getText().toString(),
									Toast.LENGTH_SHORT).show();
							ad.dismiss();
						}

					});
			ad.show();
		}

	}

	private ArrayList<RECOBeaconRegion> generateBeaconRegion() {
		ArrayList<RECOBeaconRegion> regions = new ArrayList<RECOBeaconRegion>();

		RECOBeaconRegion recoRegion;
		// recoRegion = new RECOBeaconRegion(MainActivity.RECO_UUID,"Region");
		recoRegion = new RECOBeaconRegion(MainActivity.RECO_UUID, "REGIONS");
		regions.add(recoRegion);
		Log.e("TAG", "region generate!");
		return regions;
	}

	private void connectTobeacon() {
		// TODO Auto-generated method stub
		Log.e(TAG, "connectTobeacon()");
		mRecoManager.setRangingListener(this);
		mRecoManager.bind(this);

		// tx.setText(String.valueOf(mRecoManager.isBound())+" after bind");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1 && resultCode == Activity.RESULT_CANCELED) {
			finish();
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.e(TAG, "onResume()");
		
		/*
		 * mRangingListAdapter = new RECORangingListAdapter(this);
		 * mRegionListView=(ListView)findViewById(R.id.list_ranging);
		 * mRegionListView.setAdapter(mRangingListAdapter);
		 */
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.e(TAG, "onDestroy() Stop service");
		this.stop(mRegions);
		this.unbind();
	}

	private void unbind() {
		Log.e(TAG, "unbind()");
		try {
			mRecoManager.unbind();
		} catch (RemoteException e) {
			Log.e(TAG, "Remote Exception in unbind()");
		}
	}

	@Override
	public void onServiceConnect() {
		// TODO Auto-generated method stub
		Log.e(TAG, "onServiceConnect()");
		mRecoManager.setDiscontinuousScan(false);
		this.start(mRegions);
	}

	@Override
	public void onServiceFail(RECOErrorCode arg0) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public void didRangeBeaconsInRegion(Collection<RECOBeacon> recoBeacons,
			RECOBeaconRegion recoRegion) {

		IdentifyArea ia = new IdentifyArea(); //set msgToserver
		area = ia.whereArea(recoBeacons);

		msgToServer = String.valueOf(area);
		// Toast.makeText(this, msgToServer, Toast.LENGTH_SHORT).show();
		Log.e("TAG", msgToServer);
	}

	@Override
	public void rangingBeaconsDidFailForRegion(RECOBeaconRegion arg0,
			RECOErrorCode arg1) {
		// TODO Auto-generated method stub
		return;
	}

	protected void start(ArrayList<RECOBeaconRegion> regions) {
		// TODO Auto-generated method stub
		for (RECOBeaconRegion region : regions) {
			try {
				mRecoManager.startRangingBeaconsInRegion(region);
			} catch (RemoteException e) {
				Log.i(TAG, "Remote Exception in start()");
				e.printStackTrace();
			} catch (NullPointerException e) {
				Log.i(TAG, "Null Pointer Exception");
				e.printStackTrace();
			}
		}
	}

	protected void stop(ArrayList<RECOBeaconRegion> regions) {
		// TODO Auto-generated method stub
		startService(new Intent(this,BackGroundScan.class));
		Log.e("TAG","Start background service");
		for (RECOBeaconRegion region : regions) {
			try {
				mRecoManager.stopRangingBeaconsInRegion(region);
			} catch (RemoteException e) {
				Log.i(TAG, "Remote Exception in stop()");
				e.printStackTrace();
			} catch (NullPointerException e) {
				Log.i(TAG, "Null Pointer Exception");
				e.printStackTrace();
			}
		}
	}

}

