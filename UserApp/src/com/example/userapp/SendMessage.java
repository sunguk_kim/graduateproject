package com.example.userapp;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


/*class for SendMessage to server*/

//@SuppressWarnings("deprecation")
public class SendMessage { //user
	public SendMessage(){
	}
	
	public String SendMessages(String msg){
		String result;
		result = SendByHttp(msg);
		
		Log.e("sendByHttp","send result in sendmessage class : "+result);
		String[][] parsedData = jsonParserList(result);
		if(result.equals("timeout"))
			Log.e("sendbyHttp","timeout");
		else return result;
		return null;
		
	}
	private String SendByHttp(String msg) {
		Log.e("sendbyHttp",msg);
		if(msg == null)
			msg = "";
		
		String URL = "http://172.16.38.177:8181/MemberManager/service.jsp";
		
		DefaultHttpClient client = new DefaultHttpClient();
		try {
			/* message 占쏙옙占쏙옙占쏙옙 占쏙옙占�*/
			HttpPost post = new HttpPost(URL+"?msg="+msg);

			Log.e("sendbyHttp","send "+msg); //protocol占쏙옙 占쏙옙占�response占쏙옙 처占쏙옙
			/* 占쏙옙占쏙옙占시곤옙 占쌍댐옙 5占쏙옙 */
			HttpParams params = client.getParams();
			HttpConnectionParams.setConnectionTimeout(params, 3000);
			HttpConnectionParams.setSoTimeout(params, 3000);
			
			/* 占쏙옙占쏙옙占쏙옙 占쏙옙占쏙옙 占쏙옙 占쏙옙占쏙옙占쏙옙占쏙옙 占쏙옙占쏙옙占싶몌옙 占쌨아울옙占쏙옙 占쏙옙占쏙옙 */
			HttpResponse response = client.execute(post);
			BufferedReader bufreader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent(),
							"utf-8"));

			String line = null;
			String result = "";
			while ((line = bufreader.readLine()) != null) {
				result += line;
			}

			Log.e("sendbyHttp","get resultsssss : "+result);
			return result;
		} catch (Exception e) {
			Log.e("sendbyHttp","exception occur "+e.toString());
			e.printStackTrace();
			client.getConnectionManager().shutdown();	// 占쏙옙占쏙옙 占쏙옙占쏙옙 占쏙옙占쏙옙
			return "timeout"; 
		}
		
		
	}

	/**
	 * 占쏙옙占쏙옙 JSON 占쏙옙체占쏙옙 占식쏙옙占싹댐옙 占쌨소듸옙
	 * @param page
	 * @return
	 */
	private String[][] jsonParserList(String pRecvServerPage) {
		
		Log.e("sendbyHttp","서버에서 받은 전체 내용 : "+ pRecvServerPage);
		
		try {
			JSONObject json = new JSONObject(pRecvServerPage);
			JSONArray jArr = json.getJSONArray("List");


			// 占쌨아울옙 pRecvServerPage占쏙옙 占싻쇽옙占싹댐옙 占싸븝옙
			String[] jsonName = {"msg1", "msg2", "msg3"};
			String[][] parseredData = new String[jArr.length()][jsonName.length];
			for (int i = 0; i < jArr.length(); i++) {
				json = jArr.getJSONObject(i);
				
				for(int j = 0; j < jsonName.length; j++) {
					parseredData[i][j] = json.getString(jsonName[j]);
				}
			}
			
			
			// 占쏙옙占쏙옙 占쏙옙 占쏙옙占쏙옙占싶몌옙 확占쏙옙占싹깍옙 占쏙옙占쏙옙 占싸븝옙
			for(int i=0; i<parseredData.length; i++){
				Log.i("JSON을 분석한 데이터 "+i+" : ", parseredData[i][0]);
				Log.i("JSON을 분석한 데이터 "+i+" : ", parseredData[i][1]);
				Log.i("JSON을 분석한 데이터 "+i+" : ", parseredData[i][2]);
			}

			return parseredData;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
