package com.example.userapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

/*class for CustomDialog about first login*/
public class CustomDialog extends Dialog {

	private EditText userID;
	private Button confirm;
	private View.OnClickListener dClickListener;
	
	public CustomDialog(Context context, View.OnClickListener singleListener) {
		super(context, android.R.style.Theme_Translucent_NoTitleBar);
		this.dClickListener = singleListener;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 다이얼로그 외부 화면 흐리게 표현
		WindowManager.LayoutParams lpWindow = new WindowManager.LayoutParams();
		lpWindow.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		lpWindow.dimAmount = 0.8f;
		getWindow().setAttributes(lpWindow);

		setContentView(R.layout.customdialog);

		userID = (EditText) findViewById(R.id.userID_customDlog);
		confirm = (Button) findViewById(R.id.btn_customDlog);
		
		if(dClickListener !=null){
			confirm.setOnClickListener(dClickListener);
		}
	}
}
