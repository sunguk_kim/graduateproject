package Beacon;

import java.util.Collection;

import android.util.Log;

import com.perples.recosdk.RECOBeacon;

/*class for identify area*/
public class IdentifyArea {
	public int whereArea(Collection<RECOBeacon> recoBeacons) {
		for (RECOBeacon recoBeacon : recoBeacons) {
			if(recoBeacons.size()==1)
				return recoBeacon.getMinor();
			else
				return IterateArea(recoBeacons);
		}
		return 0;
	}

	private int IterateArea(Collection<RECOBeacon> recoBeacons) {
		double value = 999999.0; //handle under flow
		String prox = "Far";
	
		// TODO Auto-generated method stub
		if(recoBeacons.size() == 2){
			Beacon[] list = new Beacon[2];
			int i=0;
				for(RECOBeacon recoBeacon : recoBeacons){
				
			 list[i++]=new Beacon(recoBeacon.getProximityUuid(),
						recoBeacon.getMajor(), recoBeacon.getMinor(),
						recoBeacon.getAccuracy(),recoBeacon.getProximity().toString());	
			
			 if(recoBeacon.getAccuracy() < 0 ){
					list[i-1].setAccurity(value);
					list[i-1].setProximity(prox);
			 }
			}
			Log.e("POS","Beacons 2 : "+list[0].getProximity()+"  "+list[1].getProximity());
			if(list[0].getProximity().contains("Immediate")&&(list[1].getProximity().contains("Near")||list[1].getProximity().contains("Far"))){
				return list[0].getMinor();
			}
			else if(list[1].getProximity().contains("Immediate")&&(list[0].getProximity().contains("Near")||list[0].getProximity().contains("Far"))){
				return list[1].getMinor();
			}
			else{
				if(list[0].getAccurity() > list[1].getAccurity())
					return list[1].getMinor();
				else
					return list[0].getMinor();
			}
			
		}
		
		else if(recoBeacons.size()==3){
			Beacon[] list = new Beacon[3];
			int i=0;
			for(RECOBeacon recoBeacon : recoBeacons){
			 list[i++]=new Beacon(recoBeacon.getProximityUuid(),
						recoBeacon.getMajor(), recoBeacon.getMinor(),
						recoBeacon.getAccuracy(),recoBeacon.getProximity().toString());	

			 if(recoBeacon.getAccuracy() < 0 ){
					list[i-1].setAccurity(value);
					list[i-1].setProximity(prox);
			 }
			}
			Log.e("POS","Beacons 3 : "+list[0].getProximity()+"  "+list[1].getProximity()+"   "+list[2].getProximity());
			if(list[0].getProximity().contains("Immediate")&&!(list[1].getProximity().contains("Immediate"))&&!(list[2].getProximity().contains("Immediate"))){
				return list[0].getMinor();
			}
			else if(list[1].getProximity().contains("Immediate")&&!(list[0].getProximity().contains("Immediate"))&&!(list[2].getProximity().contains("Immediate"))){
				return list[1].getMinor();
			}
			else if(list[2].getProximity().contains("Immediate")&&!(list[0].getProximity().contains("Immediate"))&&!(list[1].getProximity().contains("Immediate"))){
				return list[2].getMinor();
			}
			else
			{
				if(list[0].getAccurity() > list[1].getAccurity()){
					if(list[0].getAccurity() > list[2].getAccurity()){
						return list[0].getMinor();
					}
					else
						return list[2].getMinor();
				}
				else if(list[1].getAccurity() > list[2].getAccurity()){
					if(list[1].getAccurity() > list[0].getAccurity())
						return list[1].getMinor();
					else
						return list[0].getMinor();
				}
				else if(list[2].getAccurity() > list[1].getAccurity()){
					if(list[2].getAccurity() > list[0].getAccurity())
						return list[2].getMinor();
					else
						return list[0].getMinor();
				}
			}
		}
		
		return 0;
	}

}
	