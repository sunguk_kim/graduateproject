/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014-2015 Perples, Inc.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package Beacon;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.example.userapp.MainActivity;
import com.perples.recosdk.RECOBeaconManager;
import com.perples.recosdk.RECOBeaconRegion;
import com.perples.recosdk.RECOServiceConnectListener;

/**
 * RECOActivity class is the base activity for RECOMonitoringActivity and RECORangingActivity.
 * If you want to implement monitoring or ranging in a single class, 
 * you can remove this class and include the methods and RECOServiceConnectListener to each class.
 *
 * RECOActivity �대옒�ㅻ뒗 RECOMonitoringActivity��RECORangingActivity瑜��꾪븳 湲곕낯 �대옒���낅땲��
 * Monitoring �대굹 ranging���⑥씪 �대옒�ㅻ줈 援ъ꽦�섍퀬 �띠쑝�쒕떎硫� ���대옒�ㅻ� ��젣�섏떆怨��꾩슂��硫붿냼�쒖� RECOServiceConnectListener瑜��대떦 �대옒�ㅼ뿉��援ы쁽�섏떆湲�諛붾엻�덈떎.
 */
public abstract class RECOActivity extends Activity implements RECOServiceConnectListener {
	protected RECOBeaconManager mRecoManager;
	protected ArrayList<RECOBeaconRegion> mRegions;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/**
		 * Create an instance of RECOBeaconManager (to set scanning target and ranging timeout in the background.)
		 * If you want to scan RECOs only and not to set ranging timeout in the backgournd, create an instance: 
		 * 		mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(), true, false);
		 * WARNING: False enableRangingTimeout will affect the battery consumption.
		 * 
		 * RECOBeaconManager �몄뒪�댁뒪���앹꽦�⑸땲�� (�ㅼ틪 ��긽 諛�諛깃렇�쇱슫��ranging timeout �ㅼ젙)
		 * RECO留뚯쓣 �ㅼ틪�섍퀬, 諛깃렇�쇱슫��ranging timeout���ㅼ젙�섍퀬 �띠� �딆쑝�쒕떎硫� �ㅼ쓬怨�媛숈씠 �앹꽦�섏떆湲�諛붾엻�덈떎.
		 * 		mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(), true, false); 
		 * 二쇱쓽: enableRangingTimeout��false濡��ㅼ젙 �� 諛고꽣由��뚮え�됱씠 利앷��⑸땲��
		 */

		mRecoManager = RECOBeaconManager.getInstance(getApplicationContext(), MainActivity.SCAN_RECO_ONLY, MainActivity.ENABLE_BACKGROUND_RANGING_TIMEOUT);
		mRegions = this.generateBeaconRegion();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	private ArrayList<RECOBeaconRegion> generateBeaconRegion() {
		ArrayList<RECOBeaconRegion> regions = new ArrayList<RECOBeaconRegion>();
		
		RECOBeaconRegion recoRegion;
		//recoRegion = new RECOBeaconRegion(MainActivity.RECO_UUID,"Region");
		recoRegion = new RECOBeaconRegion(MainActivity.RECO_UUID,"REGIONS");
		regions.add(recoRegion);
		Log.e("TAG","region generate!");
		return regions;
	}
	
	protected abstract void start(ArrayList<RECOBeaconRegion> regions);
	protected abstract void stop(ArrayList<RECOBeaconRegion> regions);
}
