package Beacon;

public class Trigulation {
	static public Point getPosition(Beacon a,Beacon b, Beacon c){
			Point p;
	
			double bAlat = a.getPoint().getY();
		    double bAlong = a.getPoint().getX();
		    double bBlat = b.getPoint().getY();
		    double bBlong = b.getPoint().getX();
		    double bClat = c.getPoint().getY();
		    double bClong = c.getPoint().getX();

		    double W, Z, foundBeaconLat, foundBeaconLong;//, foundBeaconLongFilter;
		    W = a.getAccurity() * a.getAccurity() - b.getAccurity() * b.getAccurity() - bAlat * bAlat - bAlong * bAlong + bBlat * bBlat + bBlong * bBlong;
		    Z = b.getAccurity() * b.getAccurity() - c.getAccurity() * c.getAccurity() - bBlat * bBlat - bBlong * bBlong + bClat * bClat + bClong * bClong;

		    foundBeaconLat = (W * (bClong - bBlong) - Z * (bBlong - bAlong)) / (2 * ((bBlat - bAlat) * (bClong - bBlong) - (bClat - bBlat) * (bBlong - bAlong)));
		    foundBeaconLong = (W - 2 * foundBeaconLat * (bBlat - bAlat)) / (2 * (bBlong - bAlong));
		    //`foundBeaconLongFilter` is a second measure of `foundBeaconLong` to mitigate errors
		    //foundBeaconLongFilter = (Z - 2 * foundBeaconLat * (bClat - bBlat)) / (2 * (bClong - bBlong));

		    //foundBeaconLong = (foundBeaconLong + foundBeaconLongFilter) / 2;
		    
		    p = new Point((int)foundBeaconLat,(int)foundBeaconLong);
		    
		   // Location foundLocation = new Location("Location");
		   //   foundLocation.setLatitude(foundBeaconLat);
		   //  foundLocation.setLongitude(foundBeaconLong);


	   /* double W= 
	    		a.getAccurity()*a.getAccurity() - 
	    		b.getAccurity()*b.getAccurity() -
	    		a.getPoint().getX()*a.getPoint().getX() - 
	    		a.getPoint().getY()*a.getPoint().getY() +
	    		b.getPoint().getX()*b.getPoint().getX() +
	    		b.getPoint().getY()*b.getPoint().getY();
	    double Z = 
	    		b.getAccurity()*b.getAccurity() - 
	    		c.getAccurity()*c.getAccurity() - 
	    		b.getPoint().getX()*b.getPoint().getX() - 
	    		b.getPoint().getY()*b.getPoint().getY() + 
	    		c.getPoint().getX()*c.getPoint().getX() + 
	    		c.getPoint().getY()*c.getPoint().getY();

	    int x = (int) ((W*(c.getPoint().getY()-b.getPoint().getY()) -
	    		Z*(b.getPoint().getY()-a.getPoint().getY())) / 
	    		(2 * ((b.getPoint().getX()-a.getPoint().getX())*
	    		(c.getPoint().getY()-b.getPoint().getY()) 
	    		- (c.getPoint().getX()-b.getPoint().getX())*
	    		(b.getPoint().getY()-a.getPoint().getY()))));
	    int y = (int)((W - 2*x*(b.getPoint().getX()-a.getPoint().getX())) /
	    		(2*(b.getPoint().getY()-a.getPoint().getY())));
	    //y2 is a second measure of y to mitigate errors
	   // int y2 = (int)((Z - 2*x*(c.getPoint().getX()-b.getPoint().getX())) /
	  //  		(2*(c.getPoint().getY()-b.getPoint().getY())));

	   // y = (y + y2) / 2;
	    y/=2;
		p.setX(x);
		p.setY(y);*/
		
		return p;
	}
	
}
