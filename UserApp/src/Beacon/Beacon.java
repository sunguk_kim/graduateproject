package Beacon;

/*class for beacon data*/
public class Beacon {
	private Point point;
	private double accurity;
	private int major;
	private int minor;
	private String uuid;
	private String proximity;
	
	public Beacon(){
		point=new Point();
		accurity=0;
		major=0;
		minor=0;
		proximity = "NULL";
		uuid="NULL";		
	}
	public Beacon(String uu,int m,int mn, double acc,String prox){
		accurity=acc;
		major=m;
		minor=mn;
		uuid=uu;
		proximity = prox;
		point = new Point();
	}
	public Beacon(String uu,int m,int mn, double acc,Point p){
		accurity=acc;
		major=m;
		minor=mn;
		uuid=uu;
		point = p;
	}
	public void setAccurity(double a){
		accurity=a;
	}
	public void setPosition(int x,int y){
		point.setX(x);
		point.setY(y);
	}
	public double getAccurity(){
		return accurity;
	}
	public Point getPoint(){
		return point;
	}
	public String getUUID(){
		return uuid;
	}
	public int getMajor(){
		return major;
	}
	public int getMinor(){
		return minor;
	}
	public String getProximity() {
		return proximity;
	}
	public void setProximity(String proximity) {
		this.proximity = proximity;
	}
}
