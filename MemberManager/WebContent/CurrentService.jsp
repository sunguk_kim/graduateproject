<!DOCTYPE html>
<html lang="en">

<head>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SOS Service</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- css -->
<link href="template/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<link href="template/css/style.css" rel="stylesheet" media="screen">
<link href="template/color/default.css" rel="stylesheet" media="screen">
<script src="template/js/modernizr.custom.js"></script>
<script src="member.js"></script>
</head>
<body>
	<div class="menu-area">
		<div id="dl-menu" class="dl-menuwrapper">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li><a href="index.jsp">HOME</a></li>
				<li><a href="service.jsp">Service</a></li>
				<li><a href="#">Sub Menu</a>
				<!--  
					<ul class="dl-submenu">
						<li><a href="#">Sub menu</a></li>
						<li><a href="#">Sub menu</a></li>
					</ul>
				-->
				</li>
			</ul>
		</div>
		<!-- /dl-menuwrapper -->
	</div>
	<form action="logout.do">
	<!-- Services -->
	<section id="services" class="home-section bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2> Welcome to IPC Service</h2>
						<h4> 현재 이용하시고 계시는 서비스 페이지 입니다.</h4>

						
					</div>
				</div>
			</div>

			<!-- 웹페이지 어필하는 공간으로 사용 -->
			
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<div class="service-box wow bounceInDown" data-wow-delay="0.1s">
						<i class="fa fa-code fa-4x"></i>
						<h4></h4>
						<a class="btn btn-primary" onclick="location.href = 'logout.do'">로그아웃</a>
						
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.3s">
					<div class="service-box wow bounceInDown" data-wow-delay="0.1s">
						<i class="fa fa-cog fa-4x"></i>
						<h4></h4>
						<a class="btn btn-primary" onclick="location.href ='memberUpdate.do?userid=${loginUser.userid}'">회원정보변경</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.5s">
					<div class="service-box wow bounceInDown" data-wow-delay="0.1s">
						<i class="fa fa-desktop fa-4x"></i>
						<h4></h4>
						<a class="btn btn-primary" onclick="schedule.jsp">서비스</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.7s">
					<div class="service-box wow bounceInDown" data-wow-delay="0.1s">
						<i class="fa fa-dropbox fa-4x"></i>
						<h4></h4>
						<a class="btn btn-primary">Blank</a>
					</div>
				</div>
			</div>
		</div>
		
	</section>
	</form>
	
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
							Copyright &copy; 2015 Kang jun sik	
					</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- js -->
	<script src="template/js/jquery.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
	<script src="template/js/jquery.smooth-scroll.min.js"></script>
	<script src="template/js/jquery.dlmenu.js"></script>
	<script src="template/js/wow.min.js"></script>
	<script src="template/js/custom.js"></script>
</body>

</html>
