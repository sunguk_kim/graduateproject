<!DOCTYPE html>
<html lang="en">

<head>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">

<title>SOS Service</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- css -->
<link href="template/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<link href="template/css/style.css" rel="stylesheet" media="screen">
<link href="template/color/default.css" rel="stylesheet" media="screen">
<script src="template/js/modernizr.custom.js"></script>
<script src="member.js"></script>
</head>
<body>
	<div class="menu-area">
		<div id="dl-menu" class="dl-menuwrapper">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li><a href="index.jsp">HOME</a></li>
				<li><a href="#about">INTRODUCTION</a></li>
				<li><a href="#services">LOGIN</a></li>
				<li><a href="join.jsp">JOIN</a></li>
				<li><a href="#contact">CONTACT</a></li>
				<li><a href="http://kyl1436.dothome.co.kr/xe/freeboard">Comment Board</a> <!--  
					<ul class="dl-submenu">
						<li><a href="#">Sub menu</a></li>
						<li><a href="#">Sub menu</a></li>
					</ul>
					--></li>
			</ul>
		</div>
		<!-- /dl-menuwrapper -->
	</div>

	<!-- intro area -->
	<div id="intro">

		<div class="intro-text">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="brand">
							<h1>
								<a href="index.jsp">IPC SOS SYSTEM</a>
							</h1>
							<div class="line-spacer"></div>
							<p>
								<span>JUST USE BEACON AND SMARTPHONE</span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>



	<!-- introduction -->
	<!-- About -->
	<section id="about" class="home-section bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>What is IPC service?</h2>
						<p>IPC means "Indoor-Positioning-Customized"</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<div class="box-team wow bounceInDown" data-wow-delay="0.1s">
						<img src="template/img/Part2/pic1.png" alt=""
							class="img-responsive" />
						<h4>Beacon SMARTPHONE</h4>
						<p>Just Need your SMARTPHONE.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.3s">
					<div class="box-team wow bounceInDown">
						<img src="template/img/Part2/33.jpg" alt="" class="img-responsive" />
						<h4>For Welfare</h4>
						<p>We can provide customized Service for Disabled Person.</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.5s">
					<div class="box-team wow bounceInDown">
						<img src="template/img/Part2/pic3.jpg" alt=""
							class="img-responsive" />
						<h4>Applicable for Others</h4>
						<p>Our IPC System can applicable for others and can be
							extensible!</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3"
					data-wow-delay="0.7s">
					<div class="box-team wow bounceInDown">
						<img src="template/img/Part2/pic4.jpg" alt=""
							class="img-responsive" />
						<h4>Indoor Positioning</h4>
						<p>Manager can observe users at any time</p>
					</div>
				</div>
			</div>
		</div>
	</section>



	<!-- spacer -->
	<section id="spacer1" class="home-section spacer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="color-light">
						<h2 class="wow bounceInDown" data-wow-delay="1s">We provide
							customized Service for disabled</h2>
						<h2 class="lead wow bounceInUp" data-wow-delay="2s">Join the
							IPC Service</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- Services -->
	<section id="services" class="home-section bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>Login for Service</h2>
						<p>Enter the ID and your Password</p>

						<div class="login_page" align="center">
							<form action="login.do" method="post" name="frm">
								<table>
									<tr>
										<td><h5>ID</h5></td>
										<td><input type="text" name="userid"
											style="width: 130px; height: 30px;" value="${userid}"></td>

									</tr>
									<tr>
										<td>
											<h5>PS</h5>
										</td>
										<td><input type="password" name="pwd"
											style="width: 130px; height: 30px;"></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><input type="submit"
											value="Login" onclick="return loginCheck()"
											style="width: 90px; height: 30px;"> &nbsp;&nbsp; <input
											type="reset" value="reset" style="width: 55px; height: 30px;">
											&nbsp;&nbsp; <input type="button" value="Join"
											onclick="location.href='join.jsp'"
											style="width: 45px; height: 30px;"></td>
									</tr>
									<tr>
										<td colspan="2">${message}</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- spacer 2 -->
	<section id="spacer2" class="home-section spacer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="color-light">
						<h2 class="wow bounceInDown" data-wow-delay="1s">Install
							Beacon in Indoor</h2>
						<p class="lead wow bounceInUp" data-wow-delay="2s">You need
							install Application for Our Service</p>
					</div>
				</div>
			</div>
		</div>
	</section>


	<!-- Contact -->
	<section id="contact" class="home-section bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>Contact us</h2>
						<p>Contact via form below and we will be get in touch with you
							within 24 hours.</p>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-offset-1 col-md-10">

					<form class="form-horizontal" role="form">
						<div class="form-group">
							<div class="col-md-offset-2 col-md-8">
								<input type="text" class="form-control" id="inputName"
									placeholder="Name">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-8">
								<input type="email" class="form-control" id="inputEmail"
									placeholder="Email">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-8">
								<input type="text" class="form-control" id="inputSubject"
									placeholder="Subject">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-8">
								<textarea name="message" class="form-control" rows="3"
									placeholder="Message"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-offset-2 col-md-8">
								<button type="button" class="btn btn-theme btn-lg btn-block">Send
									message</button>
							</div>
						</div>
					</form>

				</div>


			</div>
			<div class="row mar-top30 ">
				<div class="col-md-offset-2 col-md-8">
					<h5>We're on social networks</h5>
					<ul class="social-network">
						<li><a href="#"> <span class="fa-stack fa-2x"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-facebook fa-stack-1x fa-inverse"></i>
							</span></a></li>
						<li><a href="#"> <span class="fa-stack fa-2x"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-dribbble fa-stack-1x fa-inverse"></i>
							</span></a></li>
						<li><a href="#"> <span class="fa-stack fa-2x"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-twitter fa-stack-1x fa-inverse"></i>
							</span></a></li>
						<li><a href="#"> <span class="fa-stack fa-2x"> <i
									class="fa fa-circle fa-stack-2x"></i> <i
									class="fa fa-pinterest fa-stack-1x fa-inverse"></i>
							</span></a></li>
					</ul>
				</div>
			</div>

		</div>
	</section>

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>Copyright &copy; 2015 Kang jun sik</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- js -->
	<script src="template/js/jquery.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
	<script src="template/js/jquery.smooth-scroll.min.js"></script>
	<script src="template/js/jquery.dlmenu.js"></script>
	<script src="template/js/wow.min.js"></script>
	<script src="template/js/custom.js"></script>
</body>

</html>
