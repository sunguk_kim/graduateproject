<!DOCTYPE html>
<html lang="en">

<head>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SOS Service</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- css -->
<link href="template/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<link href="template/css/style.css" rel="stylesheet" media="screen">
<link href="template/color/default.css" rel="stylesheet" media="screen">
<script src="template/js/modernizr.custom.js"></script>
<script src="member.js"></script>
</head>
<body>
	<div class="menu-area">
		<div id="dl-menu" class="dl-menuwrapper">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li><a href="index.jsp">HOME</a></li>
				<li><a href="join.jsp">JOIN</a></li>
				<li><a href="#">Sub Menu</a>
					<ul class="dl-submenu">
						<li><a href="#">Sub menu</a></li>
						<li><a href="#">Sub menu</a></li>
					</ul></li>
			</ul>
		</div>
		<!-- /dl-menuwrapper -->
	</div>

	<!-- Services -->
	<section id="services" class="home-section bg-white">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>Login for Service</h2>
						<p>Enter the ID and your Password</p>

						<div class="login_page" align="center">
							<form action="login.do" method="post" name="frm">
								<table>
									<tr>
										<td><h5>ID</h5></td>
										<td><input type="text" name="userid"
											style="width: 130px; height: 30px;" value="${userid}"></td>
									</tr>
									<tr>
										<td>
											<h5>PS</h5>
										</td>
										<td><input type="password" name="pwd"
											style="width: 130px; height: 30px;"></td>
									</tr>
									<tr>
										<td colspan="2" align="center"><input type="submit"
											value="Login" onclick="return loginCheck()"
											style="width: 90px; height: 30px;"> &nbsp;&nbsp; <input
											type="reset" value="reset" style="width: 55px; height: 30px;">
											&nbsp;&nbsp; <input type="button" value="Join"
											onclick="location.href='join.jsp'"
											style="width: 45px; height: 30px;"></td>
									</tr>
									<tr>
										<td colspan="2">${message}</td>
									</tr>
								</table>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
							Copyright &copy; 2015 Kang jun sik	
					</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- js -->
	<script src="template/js/jquery.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
	<script src="template/js/jquery.smooth-scroll.min.js"></script>
	<script src="template/js/jquery.dlmenu.js"></script>
	<script src="template/js/wow.min.js"></script>
	<script src="template/js/custom.js"></script>
</body>

</html>
