
<!DOCTYPE html>
<html lang="en">

<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>SOS Service</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<link href="template/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<link href="template/css/style.css" rel="stylesheet" media="screen">
<link href="template/color/default.css" rel="stylesheet" media="screen">
<script src="template/js/modernizr.custom.js"></script>
<script src="member.js"></script>
</head>
<body>
	<div class="menu-area">
		<div id="dl-menu" class="dl-menuwrapper">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li><a href="index.jsp">HOME</a></li>
			</ul>
		</div>
		<!-- /dl-menuwrapper -->
	</div>


	<!-- JOIN : 회원가입 페이지 -->
	<section id="works" class="home-section bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>J O I N</h2>
						<h4>You can get Service! Join now!</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div id="site_content" align="center">
						<form action="join.do" method="post" name="frm">
							<table>
								<tr>
									<td>
										<h5>Name</h5>
									</td>
									<td><input type="text" name="name" size="40"
										style="width: 120px; height: 30px;">*</td>
								</tr>
								<tr>
									<td><h5>ID</h5></td>
									<td><input type="text" name="userid" size="40"
										style="width: 120px; height: 30px;">* <input
										type="hidden" name="reid" size="20"> <input
										type="button" value="중복 체크" onclick="idCheck()"></td>
								<tr>
									<td><h5>PW</h5></td>
									<td><input type="password" name="pwd" size="40"
										style="width: 120px; height: 30px;">*</td>
								</tr>
								<tr height="30">
									<td width="80"><h5>Check</h5></td>
									<td><input type="password" name="pwd_check" size="40"
										style="width: 120px; height: 30px;">*</td>
								</tr>
								<tr>
									<td><h5>E-mail</h5></td>
									<td><input type="text" name="email" size="40"
										style="width: 120px; height: 30px;"></td>
								</tr>
								<tr>
									<td><h5>Phone</h5></td>
									<td><input type="text" name="phone" size="40"
										style="width: 120px; height: 30px;"></td>
								</tr>
								<tr>
									<td><h5>Disease info</h5></td>
									<td><input type="text" name="info" size="40"
										style="width: 120px; height: 30px;">
									</td>
								</tr>
	
								<tr>
									<td><h5>Birthday</h5></td>
									<td><input type="text" name="birth" size="40"
										style="width: 120px; height: 30px;">
									</td>
								</tr>
								
								
								<tr>
									<td><h5>Emergency Contact</h5></td>
									<td><input type="text" name="emcontact" size="40"
										style="width: 120px; height: 30px;">
									</td>
								</tr>
								
									<tr>
									<td><h5>Select Mode</h5></td>
									<td><c:choose>
											<c:when test="${mVo.admin==0}">
												<input type="radio" name="admin" value="0" checked="checked"> 일반회원
					<input type="radio" name="admin" value="1"> 관리자
					</c:when>
											<c:otherwise>
												<input type="radio" name="admin" value="0"> 일반회원
					<input type="radio" name="admin" value="1" checked="checked"> 관리자
					</c:otherwise>
										</c:choose></td>
								</tr>								
								
								
								<tr>
									<td colspan="2"><input type="submit" value="submit"
										style="width: 100px; height: 30px;"
										onclick="return joinCheck()"> &nbsp;&nbsp;&nbsp; <input
										type="reset" value="reset" style="width: 100px; height: 30px;"></td>
								</tr>

							</table>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section>


	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>Copyright &copy; 2015 Kang jun sik</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- js -->
	<script src="template/js/jquery.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
	<script src="template/js/jquery.smooth-scroll.min.js"></script>
	<script src="template/js/jquery.dlmenu.js"></script>
	<script src="template/js/wow.min.js"></script>
	<script src="template/js/custom.js"></script>
</body>

</html>
