
<!DOCTYPE html>
<html lang="en">

<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>SOS Service</title>
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<link href="template/css/bootstrap.min.css" rel="stylesheet"
	media="screen">
<link href="template/css/style.css" rel="stylesheet" media="screen">
<link href="template/color/default.css" rel="stylesheet" media="screen">
<script src="template/js/modernizr.custom.js"></script>
<script src="member.js"></script>
</head>
<body>
	<div class="menu-area">
		<div id="dl-menu" class="dl-menuwrapper">
			<button class="dl-trigger">Open Menu</button>
			<ul class="dl-menu">
				<li><a href="index.jsp">HOME</a></li>
			</ul>
		</div>
		<!-- /dl-menuwrapper -->
	</div>


	<!-- JOIN : 회원가입 페이지 -->
	<section id="works" class="home-section bg-gray">
		<div class="container">
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div class="section-heading">
						<h2>Schedule</h2>
						<h4>You can put into schedule. Enter now!</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-offset-2 col-md-8">
					<div id="site_content" align="center">
						<form action="insertScedule.do" method="post" name="frm">
							<table>
								<tr>
									<td>
										<h5>Time 09:00</h5>
									</td>
									<td><input type="text" name="t0900" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 10:00</h5>
									</td>
									<td><input type="text" name="t1000" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 11:00</h5>
									</td>
									<td><input type="text" name="t1100" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 12:00</h5>
									</td>
									<td><input type="text" name="t1200" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 13:00</h5>
									</td>
									<td><input type="text" name="t1300" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 14:00</h5>
									</td>
									<td><input type="text" name="t1400" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 15:00</h5>
									</td>
									<td><input type="text" name="t1500" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 16:00</h5>
									</td>
									<td><input type="text" name="t1600" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 17:00</h5>
									</td>
									<td><input type="text" name="t1700" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 18:00</h5>
									</td>
									<td><input type="text" name="t1800" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 19:00</h5>
									</td>
									<td><input type="text" name="t1900" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 20:00</h5>
									</td>
									<td><input type="text" name="t2000" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 21:00</h5>
									</td>
									<td><input type="text" name="t2100" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 22:00</h5>
									</td>
									<td><input type="text" name="t2200" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 23:00</h5>
									</td>
									<td><input type="text" name="t2300" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
								<tr>
									<td>
										<h5>Time 24:00</h5>
									</td>
									<td><input type="text" name="t2400" size="40"
										style="width: 250px; height: 30px;"></td>
								</tr>
								
							<tr>
									<td colspan="2">
									&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
									&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
									<input type="hidden" name="userid" value="<%=request.getParameter("userid")%>">
									<input type="submit" value="submit" style="width: 100px; height: 30px;"> 
									&nbsp;&nbsp;&nbsp; 
									 
									
									<input
										type="reset" value="reset" style="width: 100px; height: 30px;"></td>
									</tr>
								

							</table>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section>


	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>Copyright &copy; 2015 Kang jun sik</p>
				</div>
			</div>
		</div>
	</footer>

	<!-- js -->
	<script src="template/js/jquery.js"></script>
	<script src="template/js/bootstrap.min.js"></script>
	<script src="template/js/jquery.smooth-scroll.min.js"></script>
	<script src="template/js/jquery.dlmenu.js"></script>
	<script src="template/js/wow.min.js"></script>
	<script src="template/js/custom.js"></script>
</body>

</html>
