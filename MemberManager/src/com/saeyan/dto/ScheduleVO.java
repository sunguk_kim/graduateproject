package com.saeyan.dto;

public class ScheduleVO {
	private String userid;

	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	private String T0900;
	private String T1000;
	private String T1100;
	private String T1200;
	private String T1300;
	private String T1400;
	private String T1500;
	private String T1600;
	private String T1700;
	private String T1800;
	private String T1900;
	private String T2000;
	private String T2100;
	private String T2200;
	private String T2300;
	private String T2400;
	
	public String getT0900() {
		return T0900;
	}
	public void setT0900(String t0900) {
		T0900 = t0900;
	}
	public String getT1000() {
		return T1000;
	}
	public void setT1000(String t1000) {
		T1000 = t1000;
	}
	public String getT1100() {
		return T1100;
	}
	public void setT1100(String t1100) {
		T1100 = t1100;
	}
	public String getT1200() {
		return T1200;
	}
	public void setT1200(String t1200) {
		T1200 = t1200;
	}
	public String getT1300() {
		return T1300;
	}
	public void setT1300(String t1300) {
		T1300 = t1300;
	}
	public String getT1400() {
		return T1400;
	}
	public void setT1400(String t1400) {
		T1400 = t1400;
	}
	public String getT1500() {
		return T1500;
	}
	public void setT1500(String t1500) {
		T1500 = t1500;
	}
	public String getT1600() {
		return T1600;
	}
	public void setT1600(String t1600) {
		T1600 = t1600;
	}
	public String getT1700() {
		return T1700;
	}
	public void setT1700(String t1700) {
		T1700 = t1700;
	}
	public String getT1800() {
		return T1800;
	}
	public void setT1800(String t1800) {
		T1800 = t1800;
	}
	public String getT1900() {
		return T1900;
	}
	public void setT1900(String t1900) {
		T1900 = t1900;
	}
	public String getT2000() {
		return T2000;
	}
	public void setT2000(String t2000) {
		T2000 = t2000;
	}
	public String getT2100() {
		return T2100;
	}
	public void setT2100(String t2100) {
		T2100 = t2100;
	}
	public String getT2200() {
		return T2200;
	}
	public void setT2200(String t2200) {
		T2200 = t2200;
	}
	public String getT2300() {
		return T2300;
	}
	public void setT2300(String t2300) {
		T2300 = t2300;
	}
	public String getT2400() {
		return T2400;
	}
	public void setT2400(String t2400) {
		T2400 = t2400;
	}
}
