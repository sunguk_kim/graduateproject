package com.saeyan.dto;

public class MemberVO {
	private String name;
	private String userid;
	private String pwd;
	private String email;
	private String phone;
	private String disease_info;
	private String birth;
	private String emcontact;
	private String pos_x;
	private String pos_y;
	private int admin;

	@Override
	public String toString() {
		return "MemberVO [name=" + name + ", userid=" + userid + ", pwd=" + pwd + ", email=" + email + ", phone="
				+ phone + ", admin=" + admin + "]";
	}

	////
	public String getPos_x() {
		return pos_x;
	}

	public String getPos_y() {
		return pos_y;
	}

	public void setPos_x(String x) {
		this.pos_x = x;
	}

	public void setPos_y(String y) {
		this.pos_y = y;
	}

	public void set_diseaseinfo(String disease_info) {
		this.disease_info = disease_info;
	}

	public String get_diseaseinfo() {
		return this.disease_info;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getAdmin() {
		return admin;
	}

	public void setAdmin(int admin) {
		this.admin = admin;
	}
	
	public void setBirthday(String birth)
	{
		this.birth = birth;
	}
	public String getBirthday()
	{
		return this.birth;
	}
	public void setEmcontact(String emcontact)
	{
		this.emcontact = emcontact;
	}
	public String getEmcontact()
	{
		return this.emcontact;
	}
}
