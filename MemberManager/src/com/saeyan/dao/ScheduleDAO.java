package com.saeyan.dao;




//VO 클래스로 데이터베이스에서 불로온 객체를 VO 객체로 얻어오거나

//vo에 저장된 값을 데이터 베이스에 추가하는 것
//매번 이런 작업을 위해서 객체를 생성하기 보다는 이런 싱글톤 패턴을 이용한다

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.saeyan.dto.MemberVO;
import com.saeyan.dto.ScheduleVO;
public class ScheduleDAO {
	
	private static ScheduleDAO instance = new ScheduleDAO();

	public static ScheduleDAO getInstance() {
		return instance;
	}

	public Connection getConnection() throws Exception {
		Connection conn = null;
		Context initContext = new InitialContext();
		Context envContext = (Context) initContext.lookup("java:/comp/env");
		DataSource ds = (DataSource) envContext.lookup("jdbc/myoracle");
		conn = ds.getConnection();
		return conn;

	}
	public void insertMember(ScheduleVO SVO) {
		int result = -1;
		String sql = "insert into scedule values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, SVO.getUserid());
			System.out.println("SVO value : " + SVO.getUserid());
			pstmt.setString(2, null);
			pstmt.setString(3, null);
			pstmt.setString(4, null);
			pstmt.setString(5, null);
			pstmt.setString(6, null);
			pstmt.setString(7, null); 
			pstmt.setString(8, null);
			pstmt.setString(9, null);
			pstmt.setString(10, null);
			pstmt.setString(11, null);
			pstmt.setString(12, null);
			pstmt.setString(13, null);
			pstmt.setString(14, null);
			pstmt.setString(15, null);
			pstmt.setString(16, null);
			pstmt.setString(17, null);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	public int updateScedule(ScheduleVO SVO, String userid) {
		System.out.println(userid);
		int result = -1;
		//String sql = "insert into scedule values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		//HttpServletRequest request;
		//HttpServletResponse response;
		//request.setAttribute(arg0, arg1);
		
		
		
		String sql = "update scedule set T0900=?,T1000=?,T1100=?,"
				+ "T1200=?,T1300=?,T1400=?,T1500=?,T1600=?,T1700=?,T1800=?,"
				+ "T1900=?,T2000=?,T2100=?,T2200=?,T2300=?,T2400=? where userid='"+userid+"'";
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = getConnection();
			pstmt = conn.prepareStatement(sql);
		//	pstmt.setString(1, SVO.getName());
		//	pstmt.setString(3, SVO.getBirth());
		//	pstmt.setString(4, SVO.getContact());
		//	pstmt.setString(5, SVO.getInfo());
			/*
			pstmt.setString(1, SVO.getUserid());
			pstmt.setString(2, SVO.getT0900());
			pstmt.setString(3,SVO.getT1000()); 
			pstmt.setString(4,SVO.getT1100());
			pstmt.setString(5, SVO.getT1200());
			pstmt.setString(6, SVO.getT1300());
			pstmt.setString(7, SVO.getT1400());
			pstmt.setString(8, SVO.getT1500());
			pstmt.setString(9, SVO.getT1600());
			pstmt.setString(10, SVO.getT1700());
			pstmt.setString(11, SVO.getT1800());
			pstmt.setString(12, SVO.getT1900());
			pstmt.setString(13, SVO.getT2000());
			pstmt.setString(14, SVO.getT2100());
			pstmt.setString(15, SVO.getT2200());
			pstmt.setString(16, SVO.getT2300());
			pstmt.setString(17, SVO.getT2400());
			*/
			pstmt.setString(1, SVO.getT0900());
			pstmt.setString(2, SVO.getT1000());
			pstmt.setString(3, SVO.getT1100());
			pstmt.setString(4, SVO.getT1200());
			pstmt.setString(5, SVO.getT1300());
			pstmt.setString(6, SVO.getT1400());
			pstmt.setString(7, SVO.getT1500());
			pstmt.setString(8, SVO.getT1600());
			pstmt.setString(9, SVO.getT1700());
			pstmt.setString(10, SVO.getT1800());
			pstmt.setString(11, SVO.getT1900());
			pstmt.setString(12, SVO.getT2000());
			pstmt.setString(13, SVO.getT2100());
			pstmt.setString(14, SVO.getT2200());
			pstmt.setString(15, SVO.getT2300());
			pstmt.setString(16, SVO.getT2400());
			
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
				if (pstmt != null)
					pstmt.close();
				if (conn != null)
					conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	
	}
}
