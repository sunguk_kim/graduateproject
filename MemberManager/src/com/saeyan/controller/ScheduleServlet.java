package com.saeyan.controller;
/**
 * Author : Kang jun sik
 * Description : This class use when user want to join our web-site.
 * 				 user put into information to the form.
 * 			     and create user instance and Add to the Database
 */

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.saeyan.dao.ScheduleDAO;
import com.saeyan.dto.ScheduleVO;
/**
 * Servlet implementation class JoinServlet
 */
@WebServlet("/insertScedule.do")
public class ScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("Sceduleform.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		
		
		String t0900 = request.getParameter("t0900");
		String t1000 = request.getParameter("t1000");
		String t1100 = request.getParameter("t1100");
		String t1200 = request.getParameter("t1200");
		String t1300 = request.getParameter("t1300");
		String t1400 = request.getParameter("t1400");
		String t1500 = request.getParameter("t1500");
		String t1600 = request.getParameter("t1600");
		String t1700 = request.getParameter("t1700");
		String t1800 = request.getParameter("t1800");
		String t1900 = request.getParameter("t1900");
		String t2000 = request.getParameter("t2000");
		String t2100 = request.getParameter("t2100");
		String t2200 = request.getParameter("t2200");
		String t2300 = request.getParameter("t2300");
		String t2400 = request.getParameter("t2400");
		
		System.out.println("t0900 : " + t0900);
		System.out.println("t0900 : " + t0900);
		
		ScheduleVO SVo = new ScheduleVO();
		SVo.setT0900(t0900);
		SVo.setT1000(t1000);
		SVo.setT1100(t1100);
		SVo.setT1200(t1200);
		SVo.setT1300(t1300);
		SVo.setT1400(t1400);
		SVo.setT1500(t1500);
		SVo.setT1600(t1600);
		SVo.setT1700(t1700);
		SVo.setT1800(t1800);
		SVo.setT1900(t1900);
		SVo.setT2000(t2000);
		SVo.setT2100(t2100);
		SVo.setT2200(t2200);
		SVo.setT2300(t2300);
		SVo.setT2400(t2400);
		
		ScheduleDAO mDao = ScheduleDAO.getInstance();
		int result = mDao.updateScedule(SVo, request.getParameter("userid"));
		HttpSession session = request.getSession();
		if (result == 1) {
			request.setAttribute("message", "회원가입에 성공하였습니다.");
		} else {
			request.setAttribute("message", "회원가입에 실패하였습니다");
		}
		System.out.println("result value : " + result);
		RequestDispatcher dispatcher = request.getRequestDispatcher("showallpeople.jsp");
		dispatcher.forward(request, response);
	}
	
}
