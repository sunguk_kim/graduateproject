/**
 * Author : Kang jun sik
 * Description : This class use when user want to join our web-site.
 * 				 user put into information to the form.
 * 			     and create user instance and Add to the Database
 */
package com.saeyan.controller;

import javax.servlet.RequestDispatcher;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.saeyan.dao.MemberDAO;
import com.saeyan.dao.*;
import com.saeyan.dto.*;
/**
 * Servlet implementation class JoinServlet
 */
@WebServlet("/join.do")
public class JoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		RequestDispatcher dispatcher = request.getRequestDispatcher("join.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String userid = request.getParameter("userid");
		String pwd = request.getParameter("pwd");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String admin = request.getParameter("admin");
		String info = request.getParameter("info");
		String birth = request.getParameter("birth");
		String emcontact = request.getParameter("emcontact");
		
		
		MemberVO mVo = new MemberVO();
		mVo.setName(name);
		mVo.setUserid(userid);
		mVo.setPwd(pwd);
		mVo.setEmail(email);
		mVo.setPhone(phone);
		mVo.setAdmin(Integer.parseInt(admin));
		mVo.set_diseaseinfo(info);
		mVo.setEmcontact(emcontact);
		mVo.setBirthday(birth);
		MemberDAO mDao = MemberDAO.getInstance();
		
		int result = mDao.insertMember(mVo);
		HttpSession session = request.getSession();
		
		ScheduleVO SVo = new ScheduleVO();
		SVo.setUserid(userid);
		ScheduleDAO SDO = ScheduleDAO.getInstance();
		SDO.insertMember(SVo);
		
		System.out.println("JOIN에서 INSERT MEMBER : " + userid);
		
		if (result == 1) {
			session.setAttribute("userid", mVo.getUserid());
			request.setAttribute("message", "회원가입에 성공하였습니다.");
		} else {
			request.setAttribute("message", "회원가입에 실패하였습니다");
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("login.jsp");
		dispatcher.forward(request, response);
	}
	
}
