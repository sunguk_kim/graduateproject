package com.saeyan.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.saeyan.dao.MemberDAO;
import com.saeyan.dto.MemberVO;

@WebServlet("/xy.do")
public class MemberUpdateXY extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String url = "/sendandroid.jsp";

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		// TODO Auto-generated method stub
		String message = (String) request.getSession().getAttribute("recvMessage");
		System.out.println(message);

		if (message.contains("ID")) {
			String id = message.substring(4, message.length()); // cutting the
																// message
			int result = MemberDAO.getInstance().confirmID(id);

			String send = "MSG";
			String sendmessage = "OK";
			if (result == 1) // DB
			{
				request.setAttribute("message1", "OK");
				request.setAttribute("message2", "");
				request.setAttribute("message3", "");
			}
		} else if (message.contains("USERLIST")) {
			System.out.println("come");
			String DB_URL = "jdbc:oracle:thin:@127.0.0.1:1521:XE";
			String sql = null;
			String DB_user = "scott";
			String DB_password = "tiger";
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				System.out.println("드라이버 로딩 성공");
				conn = DriverManager.getConnection(DB_URL, DB_user, DB_password);
				stmt = conn.createStatement();
				sql = "SELECT * FROM Member";
				rs = stmt.executeQuery(sql);
				JSONArray arr = new JSONArray();
				String result = "";
				while (rs.next()) {

					String temp = rs.getString("NAME");
					result += temp + ",";

				}
				request.setAttribute("message1", result);
				request.setAttribute("message2", "");
				request.setAttribute("message3", "");

				System.out.println("value : " + result);
			} catch (Exception e) {
				e.printStackTrace();
			}

			/*
			
				
				
			*/

		} else if (message.contains("GETUSER")) {
			System.out.println("come");
			String DB_URL = "jdbc:oracle:thin:@127.0.0.1:1521:XE";
			String sql = null;
			String DB_user = "scott";
			String DB_password = "tiger";
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				System.out.println("드라이버 로딩 성공");
				conn = DriverManager.getConnection(DB_URL, DB_user, DB_password);
				stmt = conn.createStatement();
				sql = "SELECT * FROM Member";
				rs = stmt.executeQuery(sql);
				JSONArray arr = new JSONArray();
				String result = "";
				while (rs.next()) {

					String temp = rs.getString("userid");
					result += temp + ",";
				}

				request.setAttribute("message1", result);
				request.setAttribute("message2", "");
				request.setAttribute("message3", "");

				System.out.println("value : " + result);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//
		else if (message.contains("INFO")) {
			String info = message.substring(4);
			String DB_URL = "jdbc:oracle:thin:@127.0.0.1:1521:XE";
			String sql = null;
			String sql2 = null;
			String DB_user = "scott";
			String DB_password = "tiger";
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				System.out.println("드라이버 로딩 성공");
				conn = DriverManager.getConnection(DB_URL, DB_user, DB_password);
				stmt = conn.createStatement();
				sql = "select * from member where USERID='" + info + "'";
				sql2 = "select * from scedule where USERID='" + info + "'";
				rs = stmt.executeQuery(sql);
				String result = "";
				String result2 = "";
				String id = null;

				// 이름에 해당하는 ID를 찾는다
				while (rs.next()) {
					{

						String temp2 = rs.getString("birth");
						String temp3 = rs.getString("emcon");
						String temp4 = rs.getString("info");
						result += temp2 + ";" + temp3 + ";" + temp4;
					}
				}

				rs = null;
				rs = stmt.executeQuery(sql2);

				while (rs.next()) {
					System.out.println("userid : " + rs.getString("T0900"));
					System.out.println("userid : " + rs.getString("T1000"));
					System.out.println("userid : " + rs.getString("T1100"));
					System.out.println("userid : " + rs.getString("T1200"));
					System.out.println("userid : " + rs.getString("T1300"));
					System.out.println("userid : " + rs.getString("T1400"));
					System.out.println("userid : " + rs.getString("T1500"));
					System.out.println("userid : " + rs.getString("T1600"));
					System.out.println("userid : " + rs.getString("T1700"));
					System.out.println("userid : " + rs.getString("T1800"));
					System.out.println("userid : " + rs.getString("T1900"));
					System.out.println("userid : " + rs.getString("T2000"));
					System.out.println("userid : " + rs.getString("T2100"));
					System.out.println("userid : " + rs.getString("T2200"));
					System.out.println("userid : " + rs.getString("T2300"));
					System.out.println("userid : " + rs.getString("T2400"));

					result2 += "09" + rs.getString("T0900") + "#" + "10" + rs.getString("T1000") + "#" + "11"
							+ rs.getString("T1100") + "#" + "12" + rs.getString("T1200") + "#" + "13"
							+ rs.getString("T1300") + "#" + "14" + rs.getString("T1400") + "#" + "15"
							+ rs.getString("T1500") + "#" + "16" + rs.getString("T1600") + "#" + "17"
							+ rs.getString("T1700") + "#" + "18" + rs.getString("T1800") + "#" + "19"
							+ rs.getString("T1900") + "#" + "20" + rs.getString("T2000") + "#" + "21"
							+ rs.getString("T2100") + "#" + "22" + rs.getString("T2200") + "#" + "23"
							+ rs.getString("T2300") + "#" + "24" + rs.getString("T2400") + "#";
				}

				request.setAttribute("message1", result + ";" + result2);
				request.setAttribute("message2", "");
				request.setAttribute("message3", "");

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		else if (message.contains(("POSI"))) {
			String posi = message.substring(4, 8); // cutting the message
			String id = message.substring(13); //
			int result = MemberDAO.getInstance().confirmID(id);
			System.out.println("Sucesss");
			
			
			
			
			if (result == 1) // DB
			{
				System.out.println("1" + posi);
				System.out.println("2" + id);

				MemberDAO mDao = MemberDAO.getInstance();
				MemberVO mVo = mDao.getMember(id);
				mVo.setPos_x(posi);
				mVo.setPos_y("1");
				mDao.updateMember(mVo);

				ArrayList<String> regid = new ArrayList<String>(); // 메시지를 보낼
																	// 대상들
				// String regid =
				// "APA91bGQcKndlFqUZKsP_Jc7vQruFlOVJfOqPJwJQ5RmTuSNQ8JTVkqYKnf186TXIS1X6uPIBUBZFZXPRQDkiaaOF2MZz5wO9rWKilbbDMgMpdp2IAzRotChL4wO-leJX5IC8R0MbgJc";
				String MESSAGE_ID = String.valueOf(Math.random() % 100 + 1); // 메시지
																				// 고유
																				// ID
				boolean SHOW_ON_IDLE = true; // 기기가 활성화 상태일때 보여줄것인지
				int LIVE_TIME = 1; // 기기가 비활성화 상태일때 GCM가 메시지를 유효화하는 시간
				int RETRY = 2; // 메시지 전송실패시 재시도 횟수
				String simpleApiKey = "AIzaSyCkFxv9exH4UnUGGs59CCnCUyPR5KgxYdo"; // 가이드
																					// 1때
																					// 받은
																					// 키
				String gcmURL = "https://android.googleapis.com/gcm/send"; // 푸쉬를
																			// 요청할
																			// 구글
																			// 주소
				try {
					

					regid.add(
							"APA91bGkuku5iK4fT3vRDYZMLu_cU2aLELmFg8o-McOI5wlYsH2dPGD_kUDGmaf_QpCOGxE4aFzNWWIPiXyB1wyWM-K8sxtD0fmQMbhhOixGzwmFBSchCrYqLUPxoexIOWAGqwMmKQ_u");

					Sender sender = new Sender(simpleApiKey);
					String msg = mVo.getName() + " / " + mVo.get_diseaseinfo() + " / "+  mVo.getEmcontact();
					Message message1 = new Message.Builder().collapseKey(MESSAGE_ID).delayWhileIdle(SHOW_ON_IDLE)
							.timeToLive(LIVE_TIME).addData("msg", msg ).build();

					MulticastResult result1 = sender.send(message1, regid, RETRY);
				} catch (Exception e) {

				}
			} 
		}
			else if (message.contains("ISALERT")) {
				MemberDAO mDao = MemberDAO.getInstance();
				MemberVO mVo = new MemberVO();
				MemberVO msg = mDao.isAlert(mVo);
				request.setAttribute("message1", msg.getPos_x());
				System.out.println("message : " + msg.getPos_x());

				msg.setPos_y("0");
				mDao.updateMember(msg);

			}
		// NONE 4360 NONE aaab
		else if (message.contains("NONE")) {
			String cutm = message.substring(4, 8);
			String cutm2 = message.substring(13);

			System.out.println("1" + cutm);
			System.out.println("2" + cutm2);

		}
		// 처음 시작할때
		else if (message.contains("GCMC")) {
			/*
			 * MemberDAO mDao = MemberDAO.getInstance(); MemberVO mVo = new
			 * MemberVO(); MemberVO msg = mDao.isAlert(mVo);
			 * request.setAttribute("message1", msg.getPos_x());
			 * System.out.println("message : " + msg.getPos_x());
			 * 
			 * msg.setPos_y("0"); mDao.updateMember(msg);
			 * 
			 */
			String code = message.substring(4, message.indexOf("PHON"));
			String phoneNum = message.substring(message.indexOf("PHON") + 4);

			// 핸드폰번호로 멤버를 찾아 -> ADMIM확인 -> 긴급 연락번호에(pushcode)코드 집어넣기
			MemberDAO mDao = MemberDAO.getInstance();
			MemberVO mVo = new MemberVO();
			mVo = mDao.getMember(Integer.parseInt(phoneNum));

			System.out.println(mVo.getName());

			if (mVo.getAdmin() == 1) {
				System.out.println(mVo.getName());
				mVo.setEmcontact(code);
				// System.out.println("CODE VALue : " + code);
				// System.out.println("value 1 : " + mVo.getEmcontact());

				mDao.updateCON(mVo);
			}

			System.out.println(code + " " + phoneNum + " " + code.length());

		} else if (message.contains("AREA")) {
			String area = message.substring(4);

			String DB_URL = "jdbc:oracle:thin:@127.0.0.1:1521:XE";
			String sql = null;
			String DB_user = "scott";
			String DB_password = "tiger";
			Connection conn = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {
				Class.forName("oracle.jdbc.driver.OracleDriver");
				System.out.println("드라이버 로딩 성공");
				conn = DriverManager.getConnection(DB_URL, DB_user, DB_password);
				stmt = conn.createStatement();
				sql = "select NAME from member where POS_X='" + area + "'";
				rs = stmt.executeQuery(sql);
				String result = "";
				String id = null;

				// 이름에 해당하는 ID를 찾는다
				while (rs.next()) {
					{
						result += rs.getString("NAME") + ",";
					}
				}
				request.setAttribute("message1", result);
				request.setAttribute("message2", "");
				request.setAttribute("message3", "");

				System.out.println(result);
			} catch (Exception e) {
				e.setStackTrace(null);
			}

		}
		ServletContext context = getServletContext();
		RequestDispatcher dispatcher = context.getRequestDispatcher(url);
		dispatcher.forward(request, response);
	}
	/*
	 * protected void doPost(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException, IOException { // TODO Auto-generated
	 * method stub
	 * 
	 * 
	 * 
	 * MemberDAO mDao = MemberDAO.getInstance(); MemberVO mVo = new MemberVO();
	 * if (message.contains("ID")) { System.out.println("test1"); String id =
	 * message.substring(4, message.length()); // cutting int result =
	 * MemberDAO.getInstance().confirmID(message); System.out.println("result" +
	 * result);
	 * 
	 * 
	 * String send = "MSG"; String sendmessage = "OK"; if (result == 1) // DB {
	 * request.setAttribute("message1", "OK"); request.setAttribute("message2",
	 * ""); request.setAttribute("message3", ""); System.out.println("test1"); }
	 * }
	 * 
	 * else if (message.contains(("POSI"))) { String posi = message.substring(4,
	 * 8); // String id = message.substring(13); // aaab id int result =
	 * MemberDAO.getInstance().confirmID(message); } ServletContext context =
	 * getServletContext(); RequestDispatcher dispatcher =
	 * context.getRequestDispatcher(url); dispatcher.forward(request,response);
	 * }
	 */
}